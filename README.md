### 0. Description
Example scenario of automatic OpenStack controller nodes installation.

In OpenStack default HA scenario we need to install OpenStack controller
services at least on three nodes. We need to create MySQL galera cluster
on those three nodes. We need to install Pacemaker for cluster management
and HaProxy to direct client request to currently active OpenStack service.

This setup guaranteed OpenStack controller serves client request with minimal 
interruption (less than 15 sec - time for Peacemaker to archive new quorum 
after some node failure) or without interruption at all.

But this scenario is very complicated and over engineered for small and 
medium OpenStack deployments.

For this size deployments, for OpenStack controller we use instead of three
only two nodes. On this nodes we install KVM to create virtual machines 
with OpenStack and related services. For shared storage between this two
hosts we use DRBD9. For track node or network failure we use Raft implementation 
in etcd. We install etcd on two controller nodes and on monitoring node 
to achieve minimal three nodes quorum. And finally, for cluster management, 
we run via systemd timer script handler.sh. This script periodically check 
current etcd leader. And if it changed, moves instances to new cluster leader host. 

This approach is very simple and straightforward. In case of failure of current
cluster leader, KVM virtual machines will be started on secondary node and
comes to serve client requests within 1-2 minutes. Which are not critical
time gap for small and medium OpenStack deployments compared to complexity 
to bring cluster alive after cluster leader node failure if a combination 
of Pacemaker, HaProxy, Galera Cluster and OpenStack service 
are stuck in inconsistent state.

![Cluster Diagram](vms.svg)


### 1. Tested environments
This module developed and tested on Debian 10 Buster.


### 2. Usage
For installation procedure look at [INSTALL.md](INSTALL.md)


### 3. Known backgrounds and issues
Not found yet


### 4. Used documentation
Look at 'used documentation' section of particular module README file.