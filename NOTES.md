egrep -c '(vmx|svm)' /proc/cpuinfo


----- nova compute pxe image --------

1. create image https://evan-jones.appspot.com/software/pxeimager-scratch.html
   install nova compute and related packages
   install consul and consul template
2. place image on tftp for pxe boot
3. ip address, hostname configure on boot via dhcp 
   nodes gets static ip addresses assigned by mac
4. when node came up consul gets nova parameters from consul
   consul template creates configuration files
   when files are created consul or script restart nova services
   to bring it up with new configs
5. or maybe configuration files can be precreated by puppet for all compute nodes
   and downloaded to node during boot up process from http server for example   

-------------------------------------



