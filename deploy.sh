#!/bin/bash

USER=alex
PASSWD=alex
CODE=/home/alexeysmovzh/openstack.controller
NODE1=10.64.30.200
NODE2=10.64.30.201
NODE3=10.64.30.202

# upload code to 3 cluster nodes
rsync -r -a -v -e ssh --exclude '.*' --rsync-path='sudo rsync' --delete $CODE/* $USER@$NODE1:/etc/puppetlabs/code/environments/production &
#rsync -r -a -v -e ssh --exclude '.*' --rsync-path='sudo rsync' --delete $CODE/* $USER@$NODE2:/etc/puppetlabs/code/environments/production &
#rsync -r -a -v -e ssh --exclude '.*' --rsync-path='sudo rsync' --delete $CODE/* $USER@$NODE3:/etc/puppetlabs/code/environments/production &
rsync -r -a -v -e ssh --exclude '.*' --rsync-path='sudo rsync' --chown=puppet:puppet --delete $CODE/* $USER@10.64.30.210:/etc/puppetlabs/code/environments/production &
wait

# deploy services
ssh $USER@$NODE1 "echo $PASSWD | sudo -S /opt/puppetlabs/bin/puppet apply /etc/puppetlabs/code/environments/production/" &
#ssh $USER@$NODE1 "echo $PASSWD | sudo -S /opt/puppetlabs/bin/puppet apply /etc/puppetlabs/code/environments/production/" &
#ssh $USER@$NODE1 "echo $PASSWD | sudo -S /opt/puppetlabs/bin/puppet apply /etc/puppetlabs/code/environments/production/" &
wait

