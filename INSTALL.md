
### 0. Install OS on baremetal server

Download and install latest Debian Linux from minimal 
netinstall image

https://www.debian.org/distrib/netinst


### 1. Copy your public SSH key 

```    
    ssh-copy-id <user>@<host>
```


### 2. Disable Spectre and Meltdown patches

*This step is optional If you don't wont disable security pathes you can skip it*

```
    sudo vi /etc/default/grub

    GRUB_CMDLINE_LINUX_DEFAULT="splash quiet pti=off kpti=off spectre_v2=off spec_store_bypass_disable=off"

    sudo update-grub
    sudo init 6
```


### 3. Install depencies and Puppet agent

```
    sudo apt install wget rsync
    wget https://apt.puppetlabs.com/puppet6-release-buster.deb
    sudo dpkg -i puppet6-release-buster.deb 
    sudo apt update
    sudo apt install puppet-agent

    # add to PATH
    sudo vi /etc/environment 

    PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/opt/puppetlabs/bin"
```  


### 4. Check and disable unnecessarily services

*This step is optional. You can skip it*

```
    systemctl list-units *.service

    sudo systemctl disable ufw
    sudo systemctl disable apparmor
    sudo systemctl disable puppet
    sudo systemctl disable apt-daily.timer 
    sudo systemctl disable apt-daily-upgrade.timer
```


### 5. Create Puppet fact with server role and cluster profile

```
    vi /opt/puppetlabs/facter/facts.d/facts.yaml

    # KVM controller node
    ---
    role: openstack_controller
    profiles: 
        - etcd_controller


    # monitoring node with only etcd installed to achieve quorum
    ---
    role: monitoring
    profiles: 
        - etcd_controller        
```


### 6. Edit parameters in deploy.sh and setup services

```
    vi <repository_path>/deploy.sh

    USER=alex
    PASSWD=alex
    CODE=/home/alexeysmovzh/openstack.controller
    NODE1=10.64.30.200
    NODE2=10.64.30.201
    NODE3=10.64.30.202

    # run deploynment script
    <repository_path>/deploy.sh
```


### 7. Reboot nodes
When Puppet successfully finish its job restart all cluster nodes.

*note: for more information please look at kdc_cluster [readme](modules/kdc_cluster/README.md)*


### 8. Check cluster leader
After nodes comes up check etcd cluster leader and move it to KVM 
host if it not on one already.

