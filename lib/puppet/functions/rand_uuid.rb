# Function generates random UUID
#
require 'securerandom'

Puppet::Functions.create_function(:rand_uuid) do
  dispatch :rand_uuid do
    return_type 'String'
  end

  def rand_uuid
    SecureRandom.uuid
  end
end
