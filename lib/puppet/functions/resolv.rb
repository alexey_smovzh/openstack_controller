# does a DNS lookup and returns an array of strings of the results
#
# example:
#   resolv($::hostname)
#
require 'resolv'

Puppet::Functions.create_function(:resolv) do
  dispatch :get_ip do
    param 'String', :host
    return_type 'String'
  end

  def get_ip(host)
    tuple = Resolv.new.getaddresses(host)
    tuple[0]
  end
end
