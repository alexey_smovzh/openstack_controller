# rubocop:disable Metrics/AbcSize
# rubocop:disable Metrics/LineLength

# Function receive network as parameter in cidr format (0.0.0.0/0)
# return network and broadcast as max and min ip addresses
# eq. max and min addresses range in given network
#
#     $net = netrange('10.64.0.0/16')
#     notice($net[min_addr])
#     notice($net[max_addr])
#
#
# IP class from: http://codepad.org/o9AHsly9
#
#
class IP
  attr_reader :ip

  def initialize(ip)
    # The regex isn't perfect but I like it
    if ip.to_s =~ /\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/
      @ip = ip
    else
      # Am certain there is a much more elegant way to do this
      octet = []
      octet[0] = (ip & 0xFF000000) >> 24
      octet[1] = (ip & 0x00FF0000) >> 16
      octet[2] = (ip & 0x0000FF00) >> 8
      octet[3] = ip & 0x000000FF
      @ip = octet.join('.')
    end
  end

  def to_i
    # convert ip to 32-bit long (ie: 192.168.0.1 -> 3232235521)
    ip_split = ip.split('.')
    long = ip_split[0].to_i << 24
    long += ip_split[1].to_i << 16
    long += ip_split[2].to_i << 8
    long + ip_split[3].to_i
    # should return long automagically, yeah?
  end

  def to_s
    # This class stores the IP as a string, so we just return it as-is
    @ip
  end

  def bits
    # Count number of bits used (1).
    # This is only really useful for the network mask
    bits = 0
    octets = ip.to_s.split('.')
    octets.each { |n| bits += Math.log10(n.to_i + 1) / Math.log10(2) unless n.to_i.zero? }
    bits.to_i
  end
end

Puppet::Functions.create_function(:netrange) do
  dispatch :netrange do
    param 'String', :net
    return_type 'Hash'
  end

  def netrange(net)
    split = net.split('/')

    ipa = split[0]
    masklen = 32 - split[1].to_i

    @mask = IP.new(((0xffffffff >> masklen) << masklen)).to_i
    @min_addr = IP.new(ipa)
    @max_addr = IP.new(@min_addr.to_i | ~@mask)

    {
      'min_addr' => @min_addr.to_s,
      'max_addr' => @max_addr.to_s
    }
  end
end
