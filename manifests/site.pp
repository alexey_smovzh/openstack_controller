# Default node
#
node default {

  # if some profiles are assotiated with node - load it
  # you can set profile on 'node' Hiera hierarchy level
  $profiles = lookup('profiles', Array, 'unique', [])


  # Use puppet stages to ensure that system preparation actions
  # take place in first time. Without relying on puppet automatic 
  # resource chaining mechanism
  # 
  stage { 'first': }
  stage { 'second': }

  Stage['first'] -> Stage['second'] -> Stage['main']


  # default for all servers
  class { 'default::server': stage => 'first' }

  # specific for virtual instances or baremetal servers
  if $is_virtual {
    class { 'default::virtual': stage => 'second' }
  } else {
    class { 'default::baremetal': stage => 'second' }
  }

  # By this instruction: https://www.example42.com/2018/04/23/puppet_tutorial_part_3/
  # Each node assigns with apropriate role.
  # For now it assign via facts.yaml file, which are created by user 
  # after server OS installation.
  #
  #     $ cat /opt/puppetlabs/facter/facts.d/facts.yaml
  #        ---
  #       role: openstack_controller
  #
  # In OpenStack enviroment this facts will be collected from VM metadata
  # here is the example
  # https://bitbucket.org/alexey_smovzh/openstack_from_source/src/master/modules/openstack/files/heat_templates/puppet/
  #

  # if role is defined and class present
  if $role != undef {
    if defined("role::${role}") {
      include "role::${role}"
    }
  }

}

