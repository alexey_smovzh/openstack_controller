# Deploy Drone.io
#
class role::drone {


  include drone::server
  include drone::cli
  include drone::runner_ssh
  include drone::runner_exec


}
