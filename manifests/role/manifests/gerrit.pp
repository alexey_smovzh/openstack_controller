# Deploy Gerrit and related service
#
class role::gerrit {

  include apache2::install
  include gerrit::install

}
