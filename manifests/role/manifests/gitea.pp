# Deploy Gitea and related service
#
class role::gitea {


  include mariadb::install
  include gitea::install

  Class['mariadb::install'] -> Class['gitea::install']


}
