# Setups KVM host
# Create folder for store KVM instances data. Install required 
# packages by KVM. Install KVM and ensures service is enabled 
# and running. Disable default network as security issue.
# If in Hiera configuration provided download cloud native
# OS images and create instances. 
#
class kvm::install inherits kvm {


  # create storage folders
  file { $kvm::storage:
    ensure  => 'directory',
    recurse => true
  }

  # check if it not already defined and install depencies
  $kvm::depencies.each|$dep| {
    if !defined(Package[$dep]) {
      package { $dep:
        ensure => installed,
      }
    }
  }

  # install 
  package { $kvm::packages:
    ensure  => installed,
    require => Package[$kvm::depencies]
  }

  # ensure service is running and enabled
  service { $kvm::services:
    ensure  => running,
    enable  => true,
    require => Package[$kvm::packages]
  }

  # disable default network
  kvm::utils::network_disable { 'disable_default_libvirt_network':
    network => 'default',
    depends => Service[$kvm::services]
  }

  # if defined download OS cloud images  
  if $kvm::images != undef {

    kvm::utils::image { 'download_os_cloud_images':
      images  => $kvm::images,
      store   => $kvm::image_storage,
      depends => File[$kvm::storage]
    }

  }

  # if defined create instances
  if $kvm::instances != undef {

    # create instances data
    kvm::utils::instance { "create_instances_on_${::hostname}":
      instances        => $kvm::instances,
      image_storage    => $kvm::image_storage,
      instance_storage => $kvm::local_storage,
      interface        => $kvm::interface,
      puppet           => $kvm::puppet,
      depends          => Service[$kvm::services]
    }

    # create instance XML configuration and 'define' it on libvirt
    kvm::utils::define { "define_instances_on_${::hostname}":
      instances        => $kvm::instances,
      instance_storage => $kvm::local_storage,
      xml_storage      => $kvm::xml_storage,
      interface        => $kvm::interface,
      depends          => Kvm::Utils::Instance["create_instances_on_${::hostname}"]
    }

  }

}
