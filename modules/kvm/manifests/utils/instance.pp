# Prepare and run KVM instance
# 
# @instances        - hash with instances parameters
# @image_storage    - full path to folder with OS images
# @instance_storage - full path to folder for instance data
# @interface        - physical interface to bind virtual network
# @puppet           - Puppet server name and address
# @depends          - requirements to run this function
#
define kvm::utils::instance (

    Hash $instances,
    String $image_storage,
    String $instance_storage,
    String $interface,
    Hash $puppet,
    Any $depends

  ) {


  # ensure folder for instances exist
  if !defined(File[$instance_storage]) {
    file { $instance_storage:
      ensure => 'directory',
      owner  => 'libvirt-qemu',
      group  => 'libvirt-qemu',
    }
  }

  # create instances
  $instances.each |$instance, $value| {

    $memory = $value['memory']
    $vcpus = $value['vcpus']
    $disk = $value['disk']
    $os = $value['os']


    # puppet server are installed from cloud init script
    # all others instances are the same, just OS and puppet agent
    # services are deployed by puppet
    if $instance == $puppet['name'] {
      $cloud_init = 'puppet-server.cloud-init.erb'
    } else {
      $cloud_init = 'instance.cloud-init.erb'
    }

    # create instance folder
    file { "${instance_storage}/${instance}":
      ensure  => 'directory',
      require => [  File[$instance_storage],
                    $depends  ]
    }

    # create instance disk
    # (copy and resize cloud OS image)
    exec { "create_disk_for_${instance}":
      command  => "/usr/bin/cp ${image_storage}/${os}.qcow2 ${instance_storage}/${instance}/${instance}.qcow2 \
                && /usr/bin/qemu-img resize ${instance_storage}/${instance}/${instance}.qcow2 ${disk}",
      provider => 'shell',
      creates  => "${instance_storage}/${instance}/${instance}.qcow2",
      require  => File["${instance_storage}/${instance}"]
    }

    # copy puppet code to puppet server disk image
    if $instance == $puppet['name'] {

      # this command must run once on instance creation process and never repeated
      # if copying is successfull create flag file and check it before run command by puppet 
      exec { 'copy_puppet_code':
        command  => "/usr/bin/virt-copy-in -a ${instance_storage}/${instance}/${instance}.qcow2 \
                                          /etc/puppetlabs/code/environments/production \
                                          /root \
                  && /usr/bin/touch ${instance_storage}/${instance}/puppet_code_copied.flag",
        provider => 'shell',
        creates  => "${instance_storage}/${instance}/puppet_code_copied.flag",
        require  => Exec["create_disk_for_${instance}"]
      }

    }

    # create cloud-init config
    file { "${instance_storage}/${instance}/${instance}.cloud-init":
      ensure  => present,
      content => template("kvm/${cloud_init}"),
      require => File["${instance_storage}/${instance}"]
    }

    # create seed image
    # command runs only on notification from network or userdata config change
    exec { "create_seed_image_for_${instance}":
      command  => "/usr/bin/cloud-localds ${instance_storage}/${instance}/${instance}-seed.qcow2 \
                                          ${instance_storage}/${instance}/${instance}.cloud-init",
      provider => 'shell',
      creates  => "${instance_storage}/${instance}/${instance}-seed.qcow2",
      require  => File["${instance_storage}/${instance}/${instance}.cloud-init"]
    }

  }

}
