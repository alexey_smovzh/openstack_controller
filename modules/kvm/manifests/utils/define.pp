# Create XML file with instance configuration and load it to libvirt
# 
# @instances        - hash with instances parameters
# @instance_storage - full path to folder where instance files are located 
# @xml_storage      - full path to folder to store XML configuration files
# @interface        - physical interface to bind virtual network
# @depends          - requirements to run this function
#
define kvm::utils::define (

    Hash $instances,
    String $instance_storage,
    String $xml_storage,
    String $interface,
    Optional[Any] $depends = undef

  ) {


  # ensure folder for instances exist
  if !defined(File[$xml_storage]) {
    file { $xml_storage:
      ensure => 'directory',
      owner  => 'libvirt-qemu',
      group  => 'libvirt-qemu',
    }
  }

  # create instances
  $instances.each |$instance, $value| {

    $memory = $value['memory']
    $vcpus = $value['vcpus']
    $disk = $value['disk']
    $os = $value['os']

    $seed = "${instance_storage}/${instance}/${instance}-seed.qcow2"
    $partition = "${instance_storage}/${instance}/${instance}.qcow2"


    # create instance XML configuration
    file { "${xml_storage}/${instance}.xml":
      ensure  => present,
      # on every puppet run, functions rand_mac and rand_uuid will generate different values 
      # to omit recreation of this file on every puppet run 'replace' defined as 'false'
      replace => false,
      content => template('kvm/instance.xml.erb'),
      require => File[$xml_storage]
    }

    # define instance 
    exec { "define_instance_${instance}":
      command  => "/usr/bin/virsh define ${xml_storage}/${instance}.xml",
      provider => 'shell',
      unless   => "/usr/bin/virsh list --all | /usr/bin/grep ${instance}",
      require  => [ File["${xml_storage}/${instance}.xml"],
                    $depends  ]
    }
  }

}
