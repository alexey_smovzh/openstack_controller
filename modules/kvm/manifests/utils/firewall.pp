# Configure firewall chains for instances
# 
# @instances     - hash with instances parameters
# @depends       - requirements to run this function
#
define kvm::utils::firewall (
    Hash $instances,
    Optional[Any] $depends = undef
  ) {


  $instances.each |$instance, $value| {

    $firewall = $value['firewall']

    # firewall filter name
    # it is used in several places so we need to ensure the name is the same
    $filter = "${instance}-traffic"

    # generate uuid for firewall rule
    $uuid = rand_uuid()

  }


#  todo: create hashicorp vault instance
#        create puppet instance
#        try to reboot/shutdown/start in different combinations and check if
#        it binds to the same macvtap interface number


# todo: consider to use nftables instead libvirt firewall
#       nftables you can change in any time on the fly
#       for libvirt firewall you need to stop instance
#       in some cases this downtime can be critical
#
# $ sudo virsh domiflist mariadb-cicd
# Interface   Type     Source    Model    MAC
# ------------------------------------------------------------
# macvtap0    direct   bond-ex   virtio   52:54:00:00:7b:27
#
    # create firewall config
#    file { "${store}/${instance}/${instance}.firewall":
#      ensure => present,
#      content => template("kvm/firewall.erb"),
#      notify => Exec["define_network_filter_for_${instance}"],
#      require => File["${store}/${instance}"]
#    }

    # upload filter
#    exec { "define_network_filter_for_${instance}":
#      command => "/usr/bin/virsh nwfilter-define ${store}/${instance}/${instance}.firewall", 
#      provider => 'shell',
#      refreshonly => true,
#      unless => "/usr/bin/virsh nwfilter-list | /bin/grep ${filter}",
#      require => File["${store}/${instance}/${instance}.firewall"]
#    }

}
