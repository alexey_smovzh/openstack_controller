# Install Pacemaker
# Install packages. Create Corosync configuration file 
# and authentification key. Add infrastructure administrators
# to Pacemaker management group. Create floating ip resource.
# And configures basic pacemaker settings.
#
class pacemaker::install inherits pacemaker {

  # install packages
  package { $pacemaker::packages:
    ensure => installed,
  }

  # create corosync config
  file { '/etc/corosync/corosync.conf':
    ensure  => present,
    content => template('pacemaker/corosync.conf.erb'),
    require => Package[$pacemaker::packages],
    notify  => Service[$pacemaker::services],
  }

  # write corosync auth key
  exec { 'write_corosync_key':
    command => "/usr/bin/echo ${pacemaker::key} | /usr/bin/base64 -d > /etc/corosync/authkey",
    user    => 'root',
    group   => 'root',
    creates => '/etc/corosync/authkey',
    require => File['/etc/corosync/corosync.conf']
  }

  # add administrator to group 'haclient' to login pcs UI
  administrators::utils::group { 'enable_administrators_manage_pcs':
    admins  => $pacemaker::admins,
    group   => 'haclient',
    depends => Package[$pacemaker::packages]
  }

  # ensure service is running and enabled
  service { $pacemaker::services:
    ensure  => running,
    enable  => true,
    require => Package[$pacemaker::packages],
  }

  # Enable floating ip
  exec { 'add_floatip_res':
    command => "/usr/sbin/pcs resource create VIP ocf:heartbeat:IPaddr2 ip=${pacemaker::vip} cidr_netmask=24 op monitor interval=10s",
    unless  => '/usr/sbin/pcs status resources | /bin/grep VIP',
    require => Service[$pacemaker::services]
  }

  # todo: configure stonith
  # if fencing devices not configured we must disable STONITH
  exec { 'disable_stonith':
    command => '/usr/sbin/pcs property set stonith-enabled=false \
                                           no-quorum-policy=ignore; \
                /usr/sbin/pcs resource defaults resource-stickness=100 ',
    unless  => "/usr/sbin/pcs property show | /bin/grep 'stonith-enabled: false'",
    require => Service[$pacemaker::services]
  }

  # Set cluster properties
  # longer history for cluster debug on failure
  # reduce pacemaker internal processes interval from default 15 min
  exec { 'cluster_properties':
    command => '/usr/sbin/pcs property set pe-warn-series-max=1000 \
                                           pe-input-series-max=1000 \
                                           pe-error-series-max=1000 \
                                           cluster-recheck-interval=3m',
    unless  => "/usr/sbin/pcs property show | /bin/grep 'cluster-recheck-interval: 3m'",
    require => Service[$pacemaker::services]
  }

}
