# Init Class
#
class pacemaker {

  $packages = ['pacemaker', 'corosync', 'pcs', 'crmsh']
  $services = ['pacemaker', 'corosync', 'pcsd']

  # in pacemaker.yaml there are list of cluster nodes names without its IP addresses
  # Its IP addresses this module gets from global/network.yaml by node names
  $network = lookup('network', Hash)

  $cluster      = lookup('pacemaker', Hash)
  $cluster_name = $cluster['name']
  $vip          = $cluster['vip']
  $key          = $cluster['key']
  $nodes        = $cluster['nodes']

  # load administrator accounts to enable login to PCS UI for it
  $admins = lookup('administrators', Hash)

}

