# Enable apache2 module
# 
# @modules          - array with modules names
# @depends          - requirements to run this function
#
define apache2::utils::enable_module (

    Array $modules,
    Optional[Any] $depends = undef

  ) {


  $modules.each |$module| {

    # configure module
    exec { "configure_module_${module}_${::hostname}":
      command  => "/usr/sbin/a2enmod ${module}",
      provider => 'shell',
      unless   => "/usr/sbin/apache2ctl -M | /usr/bin/grep ${module}",
      notify   => Apache2::Utils::Restart["reload_apache_${::hostname}"],
      require  => $depends
    }
  }

  # reload configuration
  apache2::utils::restart { "reload_apache_${::hostname}": }

}



