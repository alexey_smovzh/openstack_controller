# Disable apache2 module
# 
# @module           - name of apache2 module
# @depends          - requirements to run this function
#
define apache2::utils::disable_module (

    String $module,
    Optional[Any] $depends = undef

  ) {


  # configure module
  exec { "configure_module_${::hostname}":
    command  => "/usr/sbin/a2dismod ${module}",
    provider => 'shell',
    onlyif   => "/usr/sbin/apache2ctl -M | /usr/bin/grep ${module}",
    notify   => Apache2::Utils::Restart["reload_apache_${module}_${::hostname}"],
    require  => $depends
  }

  # reload configuration
  apache2::utils::restart { "reload_apache_${module}_${::hostname}": }

}



