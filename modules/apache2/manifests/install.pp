# Install Apache2
# Ensure service enabled and running
# Disable default site installed with Apache by default
#
class apache2::install inherits apache2 {


  # install packages
  package { $apache2::packages:
    ensure => installed
  }

  # run service
  service { $apache2::services:
    ensure  => running,
    enable  => true,
    require => Package[$apache2::packages]
  }

  # disable default site
  apache2::utils::disable_site { 'disable_default_site':
    site => '000-default'
  }


}
