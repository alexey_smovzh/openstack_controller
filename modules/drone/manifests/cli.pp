# Install Drone.io cli tool
# 
class drone::cli inherits drone::install {

  # Copy binary
  file { 'copy_drone_cli_binary':
    path    => "${drone::home}/drone",
    source  => 'puppet:///modules/drone/drone',
    owner   => $drone::user,
    group   => $drone::group,
    mode    => '0755',
    recurse => true
  }

}
