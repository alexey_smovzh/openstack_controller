# Prepares system for installing Drone components.
# Creates 'drone' user and group. Drone 'home' folder.
# 
class drone::install inherits drone {


  # if group puppet exist 
  # add drone user to it
  if $facts['puppet_group'] == 'true' {
    $group = [ $drone::group, 'sudo', 'puppet' ]
  } else {
    $group = [ $drone::group, 'sudo' ]
  }


  # create drone user and group
  group { $drone::group:
    ensure => present,
    system => true
  }

  user { $drone::user:
    ensure     => 'present',
    home       => $drone::home,
    shell      => '/bin/false',
    groups     => $group,
    comment    => 'Drone Continuous Integration platform',
    password   => '*',
    managehome => true,
    require    => Group[$drone::group]
  }

}
