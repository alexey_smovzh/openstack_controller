# Install Drone.io exec runner
# 
class drone::runner_exec inherits drone::install {

  # Copy binary
  file { 'copy_drone_runner_exec_binary':
    path    => "${drone::home}/drone-runner-exec",
    source  => 'puppet:///modules/drone/drone-runner-exec',
    owner   => $drone::user,
    group   => $drone::group,
    mode    => '0755',
    recurse => true
  }

  # Allow drone user to run puppet agent with root privileges
  exec { 'add_drone_to_sudoers':
    command => '/usr/bin/echo "drone   ALL=(ALL) NOPASSWD: /opt/puppetlabs/bin/puppet" >> /etc/sudoers',
    unless  => '/usr/bin/grep drone /etc/sudoers'
  }

  # create service file
  file { '/lib/systemd/system/drone-runner-exec.service':
    ensure  => present,
    content => epp('drone/service.epp', { unit => $name }),
    notify  => Service['drone-runner-exec'],
    require => File['copy_drone_runner_exec_binary']
  }

  # ensure service is running and enabled
  service { 'drone-runner-exec':
    ensure  => running,
    enable  => true,
    require => [  Exec['add_drone_to_sudoers'],
                  File['/lib/systemd/system/drone-runner-exec.service'] ]
  }

}
