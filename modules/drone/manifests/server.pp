# Install Drone.io server
#
class drone::server inherits drone::install {


  # check if it not already defined and install depencies
  $drone::depencies.each|$dep| {
    if !defined(Package[$dep]) {
      package { $dep:
        ensure => installed,
      }
    }
  }

  # create folder for sqlite database
  file { "${drone::home}/data":
    ensure  => 'directory',
    owner   => $drone::user,
    group   => $drone::group,
    recurse => true,
    require => Package[$drone::depencies]
  }

  # Copy binaries
  file { 'copy_drone_server_binary':
    path    => "${drone::home}/drone-server",
    source  => 'puppet:///modules/drone/drone-server',
    owner   => $drone::user,
    group   => $drone::group,
    mode    => '0755',
    recurse => true,
    require => File["${drone::home}/data"]
  }

  # create service file
  file { '/lib/systemd/system/drone.service':
    ensure  => present,
    content => epp('drone/service.epp', { unit => $name }),
    notify  => Service['drone'],
    require => File['copy_drone_server_binary']
  }

  # ensure service is running and enabled
  service { 'drone':
    ensure  => running,
    enable  => true,
    require => [  File['/lib/systemd/system/drone.service'],
                  File['copy_drone_server_binary']  ]
  }

}
