# Install Drone.io SSH runnner
#
class drone::runner_ssh inherits drone::install {


  # Copy binary
  file { 'copy_drone_runner_ssh_binary':
    path    => "${drone::home}/drone-runner-ssh",
    source  => 'puppet:///modules/drone/drone-runner-ssh',
    owner   => $drone::user,
    group   => $drone::group,
    mode    => '0755',
    recurse => true
  }

  # create service file
  file { '/lib/systemd/system/drone-runner-ssh.service':
    ensure  => present,
    content => epp('drone/service.epp', { unit => $name }),
    notify  => Service['drone-runner-ssh'],
    require => File['copy_drone_runner_ssh_binary']
  }

  # ensure service is running and enabled
  service { 'drone-runner-ssh':
    ensure  => running,
    enable  => true,
    require => File['/lib/systemd/system/drone-runner-ssh.service']
  }

}
