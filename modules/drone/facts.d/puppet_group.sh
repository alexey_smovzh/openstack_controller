#!/usr/bin/env bash
#

if [ $(getent group puppet) ]; then
  echo "puppet_group=true"
else
  echo "puppet_group=false"
fi
