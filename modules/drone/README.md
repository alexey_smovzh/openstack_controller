### 0. Description
This module install Drone.io Continius Integration service.

Module consist of:
- Drone server
- Drone command line tool
- Drone runner exec
- Drone runner SSH


### 1. Tested environments
This module developed and tested on Debian 10 Buster.


### 2. Usage
Build from source

```
    # install git
    sudo apt install git build-essential -y

    # install Go
    wget https://golang.org/dl/go1.15.5.linux-amd64.tar.gz
    sudo tar -C /opt/ -xzf go1.15.5.linux-amd64.tar.gz 
    export PATH=$PATH:/opt/go/bin
    go version

    # clone repository
    git clone https://github.com/drone/drone.git
    cd drone/
    git tag -l
    git checkout tags/v1.9.2

    # build
    go build -ldflags "-extldflags \"-static\"" -o release/drone-server ./cmd/drone-server


    # clone runners code
    git clone https://github.com/drone-runners/drone-runner-exec.git --depth 1
    git clone https://github.com/drone-runners/drone-runner-ssh.git --depth 1

    # build runners
    cd drone-runner-exec/
    go build -o release/drone-runner-exec 
    cd drone-runner-ssh/
    go build -o release/drone-runner-ssh

    # copy drone binary to Puppet module
    scp <user>@<build-host>:/<drone-binary> openstack.controller/modules/drone/files
```

Provide Drone configuration options in Hiera yaml

On domain level for runners installed on all nodes

```
  lookup_options:
    drone:
      merge: deep

  drone:
    name: drone
    home: '/opt/drone'
```

On server level

```
  # Build pipelines    
  drone:
    server:
      port: 80
      # id and secret values generated during integration with Gitea
      id: '<id>'
      secret: '<secret>'    
      # git service node hostname
      git: gitea
```

And install all or just one required component

```
  include drone::server
  include drone::cli
  include drone::runner_ssh
  include drone::runner_exec
```

How to configure integration with Gitea code hosting service read [GITEA.md](GITEA.md)


### 3. Known backgrounds and issues
When puppet apply executed remotely by runner updates runner itself 
this led to error of premature runner process termination. 

Possible solutions:
- Restart pipeline again. Because drone runner already updated. Pipeline
  completes sucessfully.
- Add retry attemps to this Drone pipeline step. In this case drone restart 
  pipeline by youself.


### 4. Used documentation
Drone.io documentation: https://docs.drone.io/

Git repository: https://github.com/drone/drone
