### Configure SSH keys for Drone and Client Host

- Generate SSH keys for Drone user on Drone host. Add Client host
  to known_hosts.

```
  # On Drone host
  sudo su - drone -s /bin/bash
  ssh-keygen
  ssh-keyscan -H client.host >> .ssh/known_hosts
```

- Add drone public key to .ssh/authorized_keys on Client Host
  for 'drone' user.

```
  # On Client host
  sudo su - drone -s /bin/bash
  vi /home/drone/.ssh/authorized_keys
```

- Import drone **private** ssh key in repository settings in Drone UI
  with name 'drone_key' for example.

- Then use it in pipeline

```
  server: 
    host: client.host
    user: drone
    ssh_key:
      from_secret: drone_key
```
