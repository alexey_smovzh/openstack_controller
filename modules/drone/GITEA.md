### 0. Three hosts are described in this setup:

- Gitea host with source code
- Drone continuous integration
- Puppet server

Workflow

- developer push commit to repository hosted by Gitea
- Gitea sends web-hook to Drone Server
- Drone Exec on first pipeline step executes on Puppet server 'git clone' 
  on pushed branch
- Puppet Server downloads code from Gitea
- On next pipeline step Drone execute on Puppet host module validation tests 
  (Step not showed on diagram)
- After test are passed Drone Server on each managed node through 
  'drone_runner_exec' executes locally command 'agent apply' 
- Puppet agents on nodes download fresh code from Puppet Server and apply it


![Workflow Diagram](drone.svg)



### 1. Connect Drone to Gitea

- In Gitea Web-UI create 'drone' user with some password and local authentification source
- Login by this user
- Go to page Settings -> Applications
- Generate token with name 'drone'
- Create application with name: 'drone', redirect uri: 'http://<drone-host>/login'
- Copy 'Client ID' and 'Client Secret' and assign it to 'DRONE_GITEA_CLIENT_ID' and 
  'DRONE_GITEA_CLIENT_SECRET' constants in Hiera yaml (profile/cicd.yaml).
- Apply Puppet code on drone instance to populate 'id' and 'secret' values
- Go to web page http://\<drone-host\> and you will be redirected to Gitea login page
- Login with credentials from first step and authorize access to drone application
- Logout from Gitea, Login by normal user credentials, create repository and add 
  drone user as collaborator to new repository.
- In Drone UI now you can sync and activate repositories where drone are added as collaborator.


### 2. Configure Puppet to be able to clone repository from Gitea
- On puppet server create user 'drone' if not was created yet by 
  drone::runner_exec installation, generate SSH key-pair for it

```
  sudo useradd -m -G sudo -d /opt/drone -s /bin/false drone
  sudo su - drone -s /bin/bash
  
  # generate key with default length, otherwise it will not work
  # possible it is a bug in drone
  ssh-keygen

  # add Gitea host to known_hosts
  # Gitea ssh server listens on 2222 port
  ssh-keyscan -p 2222 -H gitea.west >> .ssh/known_hosts
```

- Login to Gitea Web-UI under 'drone' user
- Go to page 'Settings -> SSH / GPG keys -> Add key' and import public key 
  of drone user from Puppet server
