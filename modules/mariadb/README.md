### 0. Description
This module install MariaDB in single server mode.

*note: this module was writen for test enviroment and*
      *passwords provided in plain text*
      *for production environment please use Hiera eyaml*
      *or some secure vault*


### 1. Tested environments
This module developed and tested on Debian 10 Buster.


### 2. Usage
Provide MariaDB initial parameters in Hiera yaml file

```
    mariadb:
        version: 10.5
        bind: '127.0.0.1'
        password: alex

    # services database parameters
    databases: 
        giteadb:
            config: 99-giteadb
            dbuser: gitea
            dbpass: gitea
            # all values below are optional 
            characterset: utf8mb4 
            collation: utf8mb4_unicode_ci
```

Then include install class 

```
    # Install
    include mariadb::install

    # Delete
    include mariadb::delete

```

**OR with profiles**

If several instances have the different roles but must share 
the same Hiera yaml configuration. This can be archived with 
profiles. With profiles nodes can have different 'roles', 
but the same 'profile'.

Create file in Hiera node hierarhy with FQDN name of node.
Define node profile or profiles in Array. After that for 
this node will be available all parameters from Hiera 
'profile/cicd.yaml' and 'profile/runners.yaml'.

``` 
    profiles:
      - 'cicd'
      - 'runners'
``` 

Install MariaDB as usual

``` 
    include mariadb::install
``` 


### 3. Known backgrounds and issues
not found yet


### 4. Used documentation
MariaDB documentation: https://mariadb.com/kb/en/library/
