# Install MariaDB
# Install depencies, add mariadb apt repository, install mariadb 
# version defined in Hiera yaml module configuration. Enable and run
# MariaDB service. Hardens MariaDB by deleting default user, database
# limiting hosts from which root user can connect, sets root password.
# Finally, if provided in Hiera configuration creates databases.
# 
class mariadb::install inherits mariadb {


  # check if it not already defined and install depencies
  $mariadb::depencies.each|$dep| {
    if !defined(Package[$dep]) {
      package { $dep:
        ensure => installed,
      }
    }
  }

  # install mariadb apt repository
  apt::utils::apt_repository { 'mariadb':
    key        => '0xF1656F24C74CD1D8',
    file       => $mariadb::repository,
    repository => "deb [arch=amd64] http://ftp.eenet.ee/pub/mariadb/repo/${mariadb::version}/debian ${::lsbdistcodename} main",
  }

  # install packages common for all roles
  package { $mariadb::packages:
    ensure  => installed,
    require => [Apt::Utils::Apt_repository['mariadb'],
                Package[$mariadb::depencies]]
  }

  # ensure service is running and enabled
  service { $mariadb::services:
    ensure  => running,
    enable  => true,
    require => Package[$mariadb::packages]
  }

  # hardening mariadb: 
  #   remove_anonymous_users
  #   remove remote root
  #   remove test database
  #   remove privileges on test database
  #   reload privilege tables  
  exec { 'hardening_mariadb':
    command  => "/usr/bin/mariadb -u root -e \"DELETE FROM mysql.global_priv WHERE User=''\" \
              && /usr/bin/mariadb -u root -e \"DELETE FROM mysql.global_priv WHERE User='root' \
                                               AND Host NOT IN ('localhost', '127.0.0.1', '::1')\" \
              && /usr/bin/mariadb -u root -e \"DROP DATABASE IF EXISTS test\" \
              && /usr/bin/mariadb -u root -e \"DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%'\" \
              && /usr/bin/mariadb -u root -e \"FLUSH PRIVILEGES\" \
              && /usr/bin/touch ${mariadb::lock_hardening}",
    user     => 'root',
    group    => 'root',
    provider => 'shell',
    require  => Service[$mariadb::services],
    onlyif   => [ '/bin/systemctl is-active mariadb'],
    unless   => "/usr/bin/test -f ${mariadb::lock_hardening}",
  }

  # setup root password
  exec { 'set_mariadb_root_password':
    command => "/usr/bin/mysqladmin --user root password \"${mariadb::password}\" \
             && /usr/bin/touch ${mariadb::lock_password}",
    user    => 'root',
    group   => 'root',
    require => Exec['hardening_mariadb'],
    onlyif  => [ '/bin/systemctl is-active mariadb'],
    unless  => "/usr/bin/test -f ${mariadb::lock_password}",
  }


  # setup services databases
  if $mariadb::databases {

    $mariadb::databases.each |$database, $options| {

      # server configuration file
      file { "/etc/mysql/mariadb.conf.d/${options['config']}.cnf":
        ensure  => present,
        content => template('mariadb/config.erb'),
        notify  => Service[$mariadb::services],
        require => Package[$mariadb::packages]
      }

      # create database
      mariadb::utils::database { "create_${database}_database":
        dbname     => $database,
        dbrootpass => $mariadb::password,
        dbuser     => $options['dbuser'],
        dbpass     => $options['dbpass'],
        depends    => File["/etc/mysql/mariadb.conf.d/${options['config']}.cnf"]
      }

    }

  }

}
