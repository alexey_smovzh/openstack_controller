# create database, database user and grant priviledges
#
# @dbname         - database name
# @dbrootpass     - password for root database user
# @dbuser         - new user name
# @dbpass         - password for new user
# @depends        - requirements to run this function
#
define mariadb::utils::database (

    String $dbname,
    String $dbrootpass,
    String $dbuser,
    String $dbpass,
    Optional[Any] $depends = undef

  ) {

  exec { "create_database_${dbname}":
    command => "/usr/bin/mariadb -u root \
                                 -p${dbrootpass} \
                                 -e \"CREATE DATABASE ${dbname}; \
                                 GRANT ALL PRIVILEGES ON ${dbname}.* TO '${dbuser}'@'%' IDENTIFIED BY '${dbpass}'; \
                                 FLUSH PRIVILEGES;\" ",
    user    => 'root',
    group   => 'root',
    require => $depends,
    onlyif  => '/bin/systemctl is-active mariadb',
    unless  => "/usr/bin/mariadb -u root \
                               -p${dbrootpass} \
                               -e \"SHOW DATABASES;\" | /bin/grep ${dbname}",
  }
}
