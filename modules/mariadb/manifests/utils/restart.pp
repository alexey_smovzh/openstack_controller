# Restart MariaDb database server
# 
#
define mariadb::utils::restart (

  ) {


  if !defined(Exec['restart_mariadb']) {

    # restart mariadb
    exec { 'restart_mariadb':
      command     => '/usr/bin/systemctl restart mariadb',
      provider    => 'shell',
      refreshonly => true
    }

  }

}
