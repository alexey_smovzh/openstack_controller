# Delete MariDB from instance
# stops service, delete repository, configuration files and databases
# purges MariaDB and already not used dependend packages.
# Finally runs 'apt clean' 
#
class mariadb::delete inherits mariadb {

  # stop and delete services
  service { $mariadb::services:
    ensure => stopped,
    enable => false,
  }

  # delete images, containers, volumes, or customized configuration files
  file { [$mariadb::repository,
          '/etc/mysql',
          $mariadb::home ]:
    ensure  => absent,
    purge   => true,
    force   => true,
    recurse => true,
  }

  # Remove a packages and purge its config files
  package { [ $mariadb::packages,
              $mariadb::depencies ]:
    ensure => 'purged'
  }

  # Remove already unneeded depencies and dowloaded apk
  apt::utils::apt_clean { 'clean_mariadb': }

}
