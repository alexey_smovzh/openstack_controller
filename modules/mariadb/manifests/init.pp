# Init class
# 
class mariadb {


  # hiera values
  $mariadb = lookup('mariadb', Hash)
  $version = $mariadb['version']
  $bind = $mariadb['bind']
  $password = $mariadb['password']
  $databases = $mariadb['databases']

  $depencies = ['gnupg', 'software-properties-common', 'lsof']
  $packages = ["mariadb-server-${version}", "mariadb-client-${version}", 'mariadb-common']
  $services = ['mariadb']

  $repository = '/etc/apt/sources.list.d/mariadb.list'
  $home = '/var/lib/mysql'

  # files created during maridb initialization 
  # manifest check it presense to ensure that this operations are already done
  # this quite faster than make query to mariadb
  $lock_hardening = '/var/lib/mysql/cluster_initialized.lock'
  $lock_password = '/var/lib/mysql/root_password_set.lock'


}
