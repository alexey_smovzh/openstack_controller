# Init Class
#
class kdc_cluster {


  $puppet = lookup('puppet', Hash)

  $cluster = lookup('cluster', Hash)
  $interval = $cluster['interval']
  $initial = $cluster['initial']
  $volume = $cluster['volume']
  $storage = $cluster['storage']
  $image_storage = "${storage}/images"
  $xml_storage = "${storage}/xml"
  $shared_storage = "${storage}/shared"
  $images = $cluster['images']
  $instances = $cluster['instances']
  $interface = $cluster['interface']


}
