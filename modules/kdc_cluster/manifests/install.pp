# Install KDC Cluster script
# Creates handler.sh script from template. Creates systemd timer
# to run handler.sh every few seconds configured in Hiera module 
# parameters. 
# Prepare storage volume for KVM instances. Prepare and deploy 
# KVM instances itself.
#
class kdc_cluster::install inherits kdc_cluster {


  # cluster handler
  file { '/usr/sbin/handler.sh':
    ensure  => present,
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    content => template('kdc_cluster/handler.sh.erb'),
  }

  # cluster timer
  file { '/lib/systemd/system/kdc-cluster.service':
    ensure  => present,
    content => template('kdc_cluster/service.erb')
  }

  file { '/lib/systemd/system/kdc-cluster.timer':
    ensure  => present,
    content => template('kdc_cluster/timer.erb')
  }

  # ensure folder for instances exist
  file { $kdc_cluster::shared_storage:
    ensure => 'directory',
    owner  => 'libvirt-qemu',
    group  => 'libvirt-qemu'
  }

  # setup system on initial node
  # note: read facts.d/drbd_state.sh comments about this dirty hack
  if ($::hostname == $kdc_cluster::initial) and ($facts['drbd_state'] == 'Primary') {

    # mount drbd volume
    exec { "mount_${kdc_cluster::volume}_to_${kdc_cluster::shared_storage}":
      command => "/usr/bin/mount /dev/drbd/by-res/${kdc_cluster::volume}/0 ${kdc_cluster::shared_storage}",
      unless  => "/usr/bin/mountpoint ${kdc_cluster::shared_storage}",
      require => [  File[$kdc_cluster::shared_storage],
                    Class['kvm::install'],
                    Class['drbd9::install'],
                    Class['etcd::install']  ]
    }

    # download OS cloud images
    kvm::utils::image { 'kdc_download_os_cloud_images':
      images => $kdc_cluster::images,
      store  => $kdc_cluster::image_storage
    }

    # configure firewall chain
    # it should be the same on all kvm hosts
    kvm::utils::firewall { 'kdc_create_firewall_chain':
      instances => $kdc_cluster::instances
    }

    # create instance disk on primary node
    kvm::utils::instance { 'kdc_create_instances':
      instances        => $kdc_cluster::instances,
      image_storage    => $kdc_cluster::image_storage,
      instance_storage => $kdc_cluster::shared_storage,
      interface        => $kdc_cluster::interface,
      puppet           => $kdc_cluster::puppet,
      depends          => [ Exec["mount_${kdc_cluster::volume}_to_${kdc_cluster::shared_storage}"],
                            Kvm::Utils::Image['kdc_download_os_cloud_images'],
                            Kvm::Utils::Firewall['kdc_create_firewall_chain'] ],
      before           => Kvm::Utils::Define["kdc_define_instances_on_${::hostname}"]
    }
  }

  # define instances
  kvm::utils::define { "kdc_define_instances_on_${::hostname}":
    instances        => $kdc_cluster::instances,
    instance_storage => $kdc_cluster::shared_storage,
    xml_storage      => $kdc_cluster::xml_storage,
    interface        => $kdc_cluster::interface
  }

  # enable timer 
  kdc_cluster::utils::timer { 'kdc_enable_timer':
    timer   => 'kdc-cluster',
    depends => [  Kvm::Utils::Define["kdc_define_instances_on_${::hostname}"],
                  File['/usr/sbin/handler.sh'],
                  File['/lib/systemd/system/kdc-cluster.service'],
                  File['/lib/systemd/system/kdc-cluster.timer'] ]
  }

}
