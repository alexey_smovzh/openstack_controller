# Enable systemd timer
#
# @timer     - Timer name
# @depends   - requirements to run this function
#
define kdc_cluster::utils::timer (

    String $timer,
    Any $depends

) {


  exec { 'enable_timer':
    command => "/usr/bin/systemctl daemon-reload \
             && /usr/bin/systemctl enable ${timer}.timer",
    unless  => "/usr/bin/systemctl is-active --quiet ${timer}.timer",
    require => $depends
  }

}
