### 0. Description
kdc_cluster is a bash script that utilise DRBD for shared storage,
KVM to run virtual machines and etcd as implementation of raft 
consensus algoritm.

This module installs script 'handler.sh' and create systemd 
timer to run it on some defined in Hiera interval. 
Script checks if it executed on etcd cluster 
leader or not. If it on leader it checks if DRBD on this node 
promoted to primary role and shared volume mounted. Then it 
checks if KVM virtual machines from given list are running. 
And start it if it not. On etcd cluster slave node script 
checks if DRBD is in secondary role, shared volume unmounted 
and KVM virtual machines are stopped.

As understood from module name it reqires KVM, DRBD and Consul
installed and running before deployment.


### 1. Tested environments
This module developed and tested on Debian 10.


### 2. Usage
Define configuration parameters in hiera yaml

```
    # kdc cluster parameters  
    cluster:
        # run handler.sh every x seconds but less than 60
        interval: 30
        # initial node
        initial: controller0
        
        # base mountpoint folder for drbd device
        # under this root folder will be created
        # ${storage}/images     - for OS cloud images download
        # ${storage}/xml        - XML files with instances configuration
        # ${storage}/shared     - shared storage folder for mount dbrd device 
        storage: /instances
        # drbd volume name
        # module mounts dbrd resource by its name 
        volume: kvm_volume

        # physical interface to bind virtual networks
        interface: br-ex

        # OS cloud images to dowload
        images: 
            debian-10-amd64:
            link: https://cloud.debian.org/images/cloud/buster/20200928-407/debian-10-generic-amd64-20200928-407.qcow2

        # instanced to deploy        
        # note: hostname can contain letters a-z, digits 0-9 and hyphen '-'
        #       all other symbols are forbitten
        instances:
            puppet-server: *small    
            jenkins: *small
            phabricator: *small
            mariadb-cicd: *small
```
*note: for flavors description look at KVM module [documentation](../kvm/README.md)*


Install

```
    include kdc_cluster::install
```


### 3. Known backgrounds and issues
Because of cluster nodes are deployed by Puppet simultaneously there are no
way to predict who will be cluster leader. Also etcd itself does not contain 
option for point which node exactly must became a cluster leader. 
Thats why kdc_cluster just creates instances, 'handler.sh' script, system service 
and timer service but not run any of this.
When Puppet finish all cluster nodes deployment, administrator must do by 
yourself one of two options:

1. Move etcd cluster leader role to 'initial' node and start kdc-cluster.timer

```
    # on any node: show cluster leader
    $ etcdctl endpoint status --cluster

    # on any node: move leader from any node to any
    etcdctl move-leader \
        --endpoints=http://10.64.30.200:2380,http://10.64.30.201:2380,http://10.64.30.202:2380 \
        <transferee-member-id>

    # on all nodes: start kdc-cluster.timer
    systemctl start kdc-cluster.timer
```

**OR**

2. Reboot all nodes, then check etcd cluster leader and move it on any of KVM hosts
if it not on it already.
Handler.sh executed by kdc-cluster.timer then promotes drdb volume to primary on 
current cluster leader, mount it to shared storage folder and start up instances.

```
    # on any node: show cluster leader
    $ etcdctl endpoint status --cluster

    # on any node: move leader from any node to any
    etcdctl move-leader \
        --endpoints=http://10.64.30.200:2380,http://10.64.30.201:2380,http://10.64.30.202:2380 \
        <transferee-member-id>
```


### 4. Used documentation

Systemd timers: https://opensource.com/article/20/7/systemd-timers
