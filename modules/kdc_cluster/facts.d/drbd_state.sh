#!/usr/bin/env bash
#

ADM=/usr/sbin/drbdadm

# check if drbd installed and its control program exist
if test -f "$ADM"; then
    
    state=$($ADM role all | sed 's|/.*||')

    echo "drbd_state=$state"

else 
    # dirty hack!
    # facter collects facts before puppet code execution
    # and before drbd installation as well
    # by this hack we tell that drbd on host has primary role
    # on initial system installation so puppet setups system well
    # on secondary and others puppet runs drbd will be already 
    # installed and facter gets proper drbd state
    echo "drbd_state=Primary"

fi


