# Manage Hosts /ets/hosts records
#
class network::hosts inherits network {

  # delete 127.0.1.1 record
  exec { 'del_127.0.1.1':
    command => "/bin/sed -i '/127.0.1.1/d' /etc/hosts",
    onlyif  => '/bin/grep 127.0.1.1 /etc/hosts',
  }

  # add to /etc/hosts each host with static ip from 'global/network.yaml'
  $network::network.each |String $node, Hash $value| {

    $value['interfaces'].each |String $interface, Hash $params| {

      # cut off network mask 
      $ip      = regsubst($params['ip'], '\/[0-9]{1,2}', '')
      $comment = $params['comment']
      $aliases = "${node}.${::domain}"

      host { $node:
        ensure       => present,
        ip           => $ip,
        comment      => $comment,
        host_aliases => $aliases,
      }

    }

  }

}
