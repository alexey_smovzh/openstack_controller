# Create config and enable kernel attribute option
#
# @option    - config file name
# @attribute - kernel attribute name (e.q. net.ipv4.ip_forward)
# 
define network::utils::add (

  String $option,
  String $attribute,

) {

  $config = "/etc/sysctl.d/${option}.conf"

  file { $config:
    ensure  => present,
    owner   => root,
    group   => root,
    content => "${attribute} = 1",
    notify  => Exec["apply_changes_${option}"],
  }

  exec { "apply_changes_${option}":
    command     => "/sbin/sysctl -p -f ${config}",
    unless      => "/sbin/sysctl ${attribute} | /bin/grep 1",
    require     => File[$config],
    refreshonly => true
  }

}
