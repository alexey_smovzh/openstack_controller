# Delete config and disable kernel attribute option
#
# @option    - config file name
# @attribute - kernel attribute name (e.q. net.ipv4.ip_forward)
# 
define network::utils::delete (

  String $option,
  String $attribute,

) {

  $config = "/etc/sysctl.d/${option}.conf"


  file { $config:
    ensure  => absent,
    purge   => true,
    force   => true,
    recurse => true,
    notify  => Exec["apply_changes_${option}"],
  }

  exec { "apply_changes_${option}":
    command     => "/sbin/sysctl -w ${attribute}=0",
    onlyif      => "/sbin/sysctl ${attribute} | /bin/grep 1",
    refreshonly => true
  }

}
