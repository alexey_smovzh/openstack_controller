# Manage IPv6 Host settings
#
class network::ip_v6 inherits network {

  $option    = '70-disable-ipv6'
  $attribute = 'net.ipv6.conf.all.disable_ipv6'


  if $network::network[$::hostname]['ip_v6'] == true {
    # Enable IPv6
    network::utils::delete { 'enable_ip_v6':
      option    => $option,
      attribute => $attribute
    }

  } else {
    # Disable IPv6
    network::utils::add { 'disable_ip_v6':
      option    => $option,
      attribute => $attribute
    }

  }

}
