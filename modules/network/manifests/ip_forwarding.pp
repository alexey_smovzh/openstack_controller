# Manage IP Forwarding Host configuration
#
class network::ip_forwarding inherits network {

  $option    = '71-enable-ip-forwarding'
  $attribute = 'net.ipv4.ip_forward'


  if $network::network[$::hostname]['ip_forwarding'] == true {
    # Enable IP forwarding
    network::utils::add { 'enable_ip_forwarding':
      option    => $option,
      attribute => $attribute
    }

  } else {
    # Disable IP forwarding
    network::utils::delete { 'disable_ip_forwarding':
      option    => $option,
      attribute => $attribute
    }

  }

}
