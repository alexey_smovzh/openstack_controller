# Init Class
#
class puppet {


  $puppet      = lookup('puppet', Hash)
  $server      = $puppet['name']
  $address     = $puppet['address']
  $environment = $puppet['environment']
  $autosign    = $puppet['autosign']


  $link = 'https://apt.puppetlabs.com/puppet6-release-buster.deb'


  $user  = 'puppet'
  $group = $user


}
