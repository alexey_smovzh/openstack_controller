# Install Puppet Server
#
class puppet::server inherits puppet::install {


  # install 
  package { 'puppetserver':
    ensure  => installed
  }

  # ensure puppet code folder are owned by puppet
  # since puppet code are uploaded by several systems
  # sometimes they can broke folder owner and cause
  # error when Puppet agent can't find manifest on server
  # and puppet group has write permission
  file { '/etc/puppetlabs/code/':
    ensure  => directory,
    owner   => $puppet::user,
    group   => $puppet::group,
    mode    => '0775',
    recurse => true,
    require => Package['puppetserver']
  }

  if $puppet::autosign {

    # configure autosign
    file { '/etc/puppetlabs/puppet/autosign.conf':
      ensure  => present,
      content => epp('puppet/autosign.conf.epp'),
      notify  => Service['puppetserver'],
      require => Package['puppetserver']
    }

  }

  # ensure service is running and enabled
  service { 'puppetserver':
    ensure  => running,
    enable  => true,
    require => Package['puppetserver']
  }


}
