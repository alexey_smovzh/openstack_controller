# Install Puppet
# Download and install Puppet repository source
#
class puppet::install inherits puppet {


  # get package name from download link
  $package = regsubst($puppet::link, '^(.*[\\\/])', '')

  # download repository package
  exec { 'download_puppet_repository':
    command  => "/usr/bin/wget -O /tmp/${package} -q ${puppet::link}",
    provider => 'shell',
    creates  => '/etc/apt/sources.list.d/puppet6.list',
    notify   => Exec['install_puppet_repository']
  }

  # install puppet repository
  exec { 'install_puppet_repository':
    command     => "/usr/bin/dpkg -i /tmp/${package}",
    provider    => 'shell',
    notify      => Exec['update_puppet_repository'],
    refreshonly => true
  }

  # update repository sources
  exec { 'update_puppet_repository':
    command     => '/usr/bin/apt update',
    refreshonly => true
  }

}
