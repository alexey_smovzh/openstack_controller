### 0. Description
This module install Gerrit code review team collaboration tool.

This module uses HTTP authentification to restrict access to Gerrit.
Apache2 are used as reverse proxy and authentificator.
htpasswd file with user credentials generated from 'administrators' 
global Hiera infrastructure configuration section.


### 1. Tested environments
This module developed and tested on Debian 10 Buster.


### 2. Usage
Describe Gerrit configuration in appropriate Hiera yaml hierarchy level.

```
  gerrit:
    # desired Gerrit release file download URL
    link: https://gerrit-releases.storage.googleapis.com/gerrit-3.2.5.1.war
    # home folder to install Gerrit and place all working files
    home: '/opt/gerrit'
    # file path for user credentials for HTTP authentification
    htpasswd: '/opt/gerrit/etc/http.passwd'
    # http port for reverse proxy listen on
    port: 80
```

Install

*note: because apache2 are used as authentificator and reverse proxy*
*we need to install it as well*

```
  include apache2::install
  include gerrit::install
```


As first step each new user should do:

1. Login to Gerrit Web UI and add its public SSH key

User -> Settings -> SSH keys

2. Add its email address 
(without this step some requests will end with java NullPointerExeption
since they needed email address which is absent)

```
  ssh -i .ssh/id_rsa <user>@<gerrit_host> -p 29418 gerrit set-account --add-email <email> <user>
```


To start new project:

1. Create project and set permissions

Browse -> Repositories -> Create new

2. Create group of code reviewers for this repository

Browse -> Groups -> Create New -> Jenkins Reviewers

3. Add members to group

Browse -> Groups -> Create New -> Jenkins Reviewers -> Members

4. Add reviewers group to repository reviewers 

Browse -> <repository> -> Commands -> Reviewers config

5. Push code

```
  cd <repository folder>
  git init
  git add .
  
  # important! 
  # copy commit-msg hook to automaticaly add Change-Id to commit
  scp -i .ssh/id_rsa -p -P 29418 alex@gerrit:hooks/commit-msg jenkins/.git/hooks/
  chmod u+x jenkins/.git/hooks/commit-msg
  
  git commit -m "Initial commit"
  git remote add origin ssh://alex@gerrit:29418/jenkins
  git push origin HEAD:refs/for/master
```


6. To disable direct push to repository and avoiding code review 

 - remove push permission from refs/heads/*
 - create refs 'refs/for/*'
 - add push permission to 'refs/for/*'



### 3. Known backgrounds and issues
none


### 4. Used documentation
https://gerrit.cloudera.org/Documentation/install-quick.html

https://gerrit-review.googlesource.com/Documentation/config-gerrit.html

https://gerrit-review.googlesource.com/Documentation/intro-project-owner.html
