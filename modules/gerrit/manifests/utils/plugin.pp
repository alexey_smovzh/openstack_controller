# Manage Gerrit plugins
# 
# @plugins          - Hash of plugins
# @home             - full path to Gerrit home folder
# @reload           - service to notify about changes
# @depends          - requirements to run this function
#
define gerrit::utils::plugin (

    Hash $plugins,
    String $home,
    Optional[Any] $reload = undef,
    Optional[Any] $depends = undef

  ) {


  $plugins.each |$plugin, $link| {

    if $link != false {

      # download plugin
      exec { "install_${plugin}":
        command  => "/usr/bin/wget -O ${home}/plugins/${plugin}.jar ${link}",
        provider => 'shell',
        user     => 'gerrit',
        group    => 'gerrit',
        creates  => "${home}/plugins/${plugin}.jar",
        notify   => $reload,
        require  => Exec['initialize_gerrit']
      }

    } else {

      # disable plugin
      exec { "disable_${plugin}":
        command  => "/usr/bin/mv ${home}/plugins/${plugin}.jar ${home}/plugins/${plugin}.disabled",
        provider => 'shell',
        user     => 'gerrit',
        group    => 'gerrit',
        creates  => "${home}/plugins/${plugin}.disabled",
        notify   => $reload,
        require  => Exec['initialize_gerrit']
      }

    }

  }

}
