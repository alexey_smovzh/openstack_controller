# Install Gerrit
# Install packages required by Gerrit. Create 'gerrit' 
# user and group. Download Gerrit binary and unpack it 
# to home folder. Init Gerrit installation. Manages 
# plugins installind additional and disabling already 
# installed. Create configuration file. Create systemd
# service file and ensure service are running and enabled.
# Configure Apache2 as reverse-proxy to provide HTTP 
# authentification for Gerrit users.
#
class gerrit::install inherits gerrit {


  # check if it not already defined and install depencies
  $gerrit::depencies.each|$dep| {
    if !defined(Package[$dep]) {
      package { $dep:
        ensure => installed,
      }
    }
  }

  # create gerrit user and group
  group { $gerrit::group:
    ensure  => present,
    system  => true,
    require => Package[$gerrit::depencies]
  }

  user { $gerrit::user:
    ensure     => 'present',
    home       => $gerrit::home,
    shell      => '/bin/false',
    groups     => $gerrit::group,
    comment    => 'Gerrit codereview tool',
    password   => '*',
    managehome => true,
    require    => Group[$gerrit::group]
  }

  # download gerrit
  exec { 'download_gerrit':
    command  => "/usr/bin/wget -O ${gerrit::home}/gerrit.war ${gerrit::link}",
    provider => 'shell',
    user     => $gerrit::user,
    group    => $gerrit::group,
    creates  => "${gerrit::home}/gerrit.war",
    require  => User[$gerrit::user]
  }

  # initialize 
  exec { 'initialize_gerrit':
    command  => "/usr/bin/java -jar ${gerrit::home}/gerrit.war init \
                                    --batch \
                                    -d ${gerrit::home} \
                                    --install-all-plugins",
    provider => 'shell',
    user     => $gerrit::user,
    group    => $gerrit::group,
    creates  => "${gerrit::home}/bin/gerrit.war",
    require  => Exec['download_gerrit']
  }

  # manage plugins
  gerrit::utils::plugin { 'manage_plugins':
    plugins => $gerrit::plugins,
    home    => $gerrit::home,
    reload  => Service[$gerrit::services],
    depends => Exec['initialize_gerrit']
  }

  # replace config
  file { "${gerrit::home}/etc/gerrit.config":
    ensure  => present,
    content => template('gerrit/gerrit.config.erb'),
    owner   => $gerrit::user,
    group   => $gerrit::group,
    notify  => Service[$gerrit::services],
    require => Gerrit::Utils::Plugin['manage_plugins']
  }

  # create service file
  file { '/lib/systemd/system/gerrit.service':
    ensure  => present,
    content => template('gerrit/gerrit.service.erb'),
    notify  => Service[$gerrit::services],
    require => File["${gerrit::home}/etc/gerrit.config"]
  }

  # ensure service is running and enabled
  service { $gerrit::services:
    ensure  => running,
    enable  => true,
    require => File['/lib/systemd/system/gerrit.service']
  }


  # configure apache2 as reverce proxy and as http authentificator
  #
  # create .htpasswd file
  administrators::utils::htpasswd { "create_${gerrit::htpasswd}":
    admins  => $gerrit::admins,
    path    => $gerrit::htpasswd,
    depends => Exec['initialize_gerrit']
  }

  # enable apache2 proxy module
  apache2::utils::enable_module { 'enable_mod_proxy':
    modules => ['proxy_http'],
    depends => Exec['initialize_gerrit']
  }

  # configure apache reverse proxy
  apache2::utils::site { 'configure_reverse_proxy':
    site    => "${::hostname}.${::domain}",
    config  => template('gerrit/gerrit.site.erb'),
    depends => [  Apache2::Utils::Enable_module['enable_mod_proxy'],
                  Administrators::Utils::Htpasswd["create_${gerrit::htpasswd}"] ]
  }

}
