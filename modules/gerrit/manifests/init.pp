# Init Class
#
class gerrit {


  $depencies = ['git', 'gitweb', 'default-jre', 'wget']
  $services = ['gerrit']

  $user = 'gerrit'
  $group = $user

  # get Hiera data
  $gerrit = lookup('gerrit', Hash)
  $link = $gerrit['link']
  $home = $gerrit['home']
  $htpasswd = $gerrit['htpasswd']
  $port = $gerrit['port']
  $plugins = $gerrit['plugins']

  # to generate password file for http authentification
  $admins = lookup('administrators', Hash)


}
