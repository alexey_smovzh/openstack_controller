### 0. Description
This module install etcd distributed key-value store.


### 1. Tested environments
This module developed and tested on Debian 10 Buster.


### 2. Usage
Describe cluster configuration in Hiera yaml file

```
  etcd:
    # cluster name
    datacenter: 'controller'
    # home folder for user etcd and folder to store etcd binaries
    home: '/opt/etcd'
    # folder for etcd data
    data: '/var/lib/etcd'
    # link to download current etcd binaries
    link: 'https://storage.googleapis.com/etcd/v3.4.13/etcd-v3.4.13-linux-amd64.tar.gz'
    # cluster members hostnames
    nodes:
      - controller0
      - controller1
      - monitoring0
```

*note: in 'nodes' section cluster members are defined by names*
*to work properly etcd needed preconfigured node IP address by hostname resolution*

Install
```
  include etcd::install
```

Some usefull commands
```
  # show cluster leader
  $ etcdctl endpoint status --cluster

  # move leader from local node to any other
  $ etcdctl move-leader <transferee-member-id>

  # move leader from any node to any
  etcdctl move-leader \
      --endpoints=http://10.64.30.200:2380,http://10.64.30.201:2380,http://10.64.30.202:2380 \
      <transferee-member-id>

  # endpoint status
  $ etcdctl endpoint health

  # get some cluster values by rest api
  curl -s http://10.64.30.200:2380/members
  curl -s http://10.64.30.200:2379/metrics

```


### 3. Known backgrounds and issues
not found


### 4. Used documentation

