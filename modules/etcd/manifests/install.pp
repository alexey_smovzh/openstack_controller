# Install Etcd
# Install packages required by Etcd. Creates 'etcd' user and group.
# Creates home and data folders. Download etcd binary by link 
# provided in Hiera configuration. Install it. Create systemd service
# file with etcd configuration passed as daemon command line parameters.
# Ensures service enabled and running.
#
class etcd::install inherits etcd {


  # check if it not already defined and install depencies
  $etcd::depencies.each|$dep| {
    if !defined(Package[$dep]) {
      package { $dep:
        ensure => installed,
      }
    }
  }

  # create etcd user and group
  group { $etcd::group:
    ensure => present,
    system => true
  }

  user { $etcd::user:
    ensure     => 'present',
    home       => $etcd::home,
    shell      => '/bin/false',
    groups     => $etcd::group,
    comment    => 'etcd distributed reliable key-value store',
    password   => '*',
    managehome => true,
    require    => Group[$etcd::group]
  }

  # create folder for store etcd data
  file { $etcd::data:
    ensure  => 'directory',
    owner   => $etcd::user,
    group   => $etcd::group,
    recurse => true,
    require => User[$etcd::user]
  }

  # extract archive name from download link
  $archive = regsubst($etcd::link, '^(.*[\\\/])', '')
  # install etcd 
  exec { 'install_etcd':
    command  => "/usr/bin/wget -q ${etcd::link} \
              && /usr/bin/tar xf ${archive} --strip-components 1",
    provider => 'shell',
    cwd      => $etcd::home,
    user     => $etcd::user,
    group    => $etcd::group,
    creates  => "${etcd::home}/etcd",
    require  => Package[$etcd::depencies]
  }

  # add etcd folder to PATH
  file { '/etc/profile.d/etcd.sh':
    ensure  => present,
    owner   => 'root',
    group   => 'root',
    content => "export PATH=\"\${PATH}:${etcd::home}\"",
    require  => Exec['install_etcd']
  }

  # create service file
  file { '/lib/systemd/system/etcd.service':
    ensure  => present,
    content => template('etcd/etcd.service.erb'),
    notify  => Service[$etcd::services],
    require => Exec['install_etcd']
  }

  # ensure service is running and enabled
  service { $etcd::services:
    ensure  => running,
    enable  => true,
    require => [  File['/lib/systemd/system/etcd.service'],
                  File[$etcd::data] ]
  }

}
