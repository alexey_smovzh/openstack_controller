# Init Class
#
class etcd {

  $depencies = ['wget']
  $services = ['etcd']

  $user = 'etcd'
  $group = $user

  $etcd = lookup('etcd', Hash)
  $datacenter = $etcd['datacenter']
  $home = $etcd['home']
  $data = $etcd['data']
  $link = $etcd['link']
  $nodes = $etcd['nodes']

}
