# Prepares system to install CFSSL
#
# Create user, group, home folder, add it to path,
# create folder for certificates
#
class cfssl::install inherits cfssl {


  # create cfssl user and group
  group { $cfssl::group:
    ensure => present,
    system => true
  }

  user { $cfssl::user:
    ensure     => 'present',
    home       => $cfssl::home,
    shell      => '/bin/false',
    groups     => $cfssl::group,
    comment    => 'CloudFlares PKI/TLS toolkit',
    password   => '*',
    managehome => true,
    require    => Group[$cfssl::group]
  }

  # add cfssl folder to PATH
  file { '/etc/profile.d/cfssl.sh':
    ensure  => present,
    owner   => 'root',
    group   => 'root',
    content => "export PATH=\"\${PATH}:${cfssl::home}\"",
    require => User[$cfssl::user]
  }

}
