# Download CFSSL binary 
# 
# @link         - URL for CFSSL binary
# @bin          - full path to /bin directory
# @earlier      - run this function befor action in earlier field
# @depends      - requirements to run this function
#
define cfssl::utils::download (

    String $link,
    String $bin,
    String $user,
    String $group,
    Optional[Any] $earlier = undef,
    Optional[Any] $depends = undef

  ) {


  # extract downloaded file name from url
  $file_long = regsubst($link, '^(.*[\\\/])', '')
  # remove version and architecture information from file name
  $file_short =regsubst($file_long, '\_.*', '')

  # download
  exec { $link:
    command  => "/usr/bin/wget -q ${link} \
              && /usr/bin/mv ${file_long} ${file_short} \
              && /usr/bin/chmod +x ${file_short}",
    provider => 'shell',
    cwd      => $bin,
    user     => $user,
    group    => $group,
    creates  => "${bin}/${file_short}",
    before   => $earlier,
    require  => $depends
  }


}
