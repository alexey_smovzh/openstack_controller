# Init class
#
class cfssl {


  # Hiera data
  $ca = lookup('ca', Hash)
  $home            = $ca['home']
  $server          = $ca['name']
  # port statically defined and can not be choosen by user
  $port            = '8888'
  # token for authentification between cfssl-server and certmanager
  # also statically defined and can not be choosed
  # openssl dgst -sha256 -hmac -hex <<< "very secret key"
  $token           = 'd0b180d0a917dcbff27549cda378bd0c4e787536917ce85a6d06b6e3f9f6546f'
  # cert data
  $ca_name         = $ca['authority']
  $ca_country      = $ca['country']
  $ca_country_code = $ca['code']
  $ca_city         = $ca['city']
  $ca_organization = $ca['organization']
  $ca_unit         = $ca['unit']
  $ca_root_ttl     = $ca['root_ttl']

  # services certificates
  $certificates    = $ca['certificates']


  $user   = 'cfssl'
  $group  = $user


  # modules from release v1.5.0
  # https://github.com/cloudflare/cfssl/releases/tag/v1.5.0
  $binaries = [
    'https://github.com/cloudflare/cfssl/releases/download/v1.5.0/cfssl-certinfo_1.5.0_linux_amd64',
    'https://github.com/cloudflare/cfssl/releases/download/v1.5.0/cfssljson_1.5.0_linux_amd64',
    'https://github.com/cloudflare/cfssl/releases/download/v1.5.0/cfssl_1.5.0_linux_amd64',
    'https://github.com/cloudflare/cfssl/releases/download/v1.5.0/mkbundle_1.5.0_linux_amd64'
  ]

  # cfssl certmanager binary
  $certmgr = 'https://github.com/cloudflare/certmgr/releases/download/v3.0.3/certmgr-linux-amd64-v3.0.3.tar.gz'


}
