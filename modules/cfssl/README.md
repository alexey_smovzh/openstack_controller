### 0. Description
This module install CloudFlare's PKI/TLS toolkit.


server.pp - creates root certificate and certificate bundle 
with name <ca_unit>.crt to import on clients. You should 
copy this bundle to some place from where clients can download 
and import it.

Also it setup cfssl as network server. To create and renew 
certificates from remote nodes with CFSSL certificate manager.

manager.pp - install cfssl certificate manager tool to automatically
create and renew managed services certificates.


### 1. Tested environments
This module developed and tested on Debian 10 Buster.


### 2. Usage

#### Setup CFSSL Server

Define CA configuration in Hiera yaml

On high level, probably on domain level, describe global certificate parameters

*note: To renew root certificate just delete (previously backup of course) /opt/cfssl/root/ folder*
*with current root certificate. Then run puppet apply and server.pp manifest*
*generate new root ca. Then you just need to update <ca_unit>.crt from new root/*
*on all clients.*

```
    lookup_options:
      ca:
        merge: deep

    # root certificate authority
    ca:
      # Root CA Server name
      name: root-ca.west
      # cert data
      authority:    'West site Certificate Authority'    # CN
      country:      'Ukraine'                            # L
      code:         'UA'                                 # C
      city:         'Kiev'                               # ST
      organization: 'West'                               # O
      unit:         'west'                               # OU
      root_ttl:     '87600h'                             # root certificate live time 10 years
```

On low level, on role, profile or node level define CFSSL server parameters

```
    # Merge strategy - 'deep'
    # Look at 'domain/west' Hiera yaml
    ca:
      # path for cfssl binaries installation
      home: '/opt/cfssl'
```

And install CFSSL server

```
    include cfssl::server
```


#### Setup Client

In this example of CFSSL client installation we will use Apache2 web-server.
For another service setup can be different but very close to this one.

At this step we already have CFSSL CA Server defined in Hiera yaml. So we
already have global certificate parameters defined and does not need to repeat it.

Define service certificate parameters on low Hiera hierarchy level, 
on role, profile or node level.

```
    # Merge strategy - 'deep'
    # Look at 'domain/west' Hiera yaml
    ca:
      # path for cfssl binaries installation
      home: '/opt/cfssl'
      # one or multiple certificates definition   
      certificates:
        # service name
        apache2:
          # service PKI profile 
          # to find details or to add new profile
          # look at templates/ca-config.json.epp
          profile: 'service'
          # path where certmgr should place service certificates
          path: '/opt/cfssl/apache2'
          # certificate folder owner and group 
          # to allow service user to access certificate files
          owner: 'www-data'
          group: 'www-data'
        gitea:
          profile: 'service'
          path: '/opt/cfssl/gitea'
          owner: 'gitea'
          group: 'gitea'
```

Create Apache2 site configuration

```
    # All requests to HTTP port redirect to HTTPS
    <VirtualHost *:80>
      ServerName site.com
      ServerAlias www.site.com
      Redirect permanent / https://site.com/
    </VirtualHost>

    # Configure HTTPS Site
    <VirtualHost *:443>
        ServerName site.com
        ServerAlias www.site.com
        SSLEngine on
        SSLCertificateFile "/opt/cfssl/apache2/ca.pem"
        SSLCertificateKeyFile "/opt/cfssl/apache2/ca-key.pem"

        DocumentRoot /var/www/html/

        # require a client certificate which has to be directly
        # signed by our CA certificate in ca.crt
        #SSLVerifyClient require
        #SSLVerifyDepth 1
        #SSLCACertificateFile "/opt/cfssl/apache2/ca.crt"
    </VirtualHost>
```

Install Apache2 and CFSSL certmgr

```
    include cfssl::manager
    include apache2::install

    Class['cfssl::manager'] -> Class['apache2::install']
```

This will setup certmgr as systemd service with name 'cfssl-mgr-apache2'
e.q. 'cfssl-mgr-\<managed service name\>'.

If you have some issues with certmgr service. You can check its configuration
and configuration of certificate PKI specification as well

```
    certmgr check \
              -f apache2/certmgr.yaml \
              -d apache2/apache2.json
```


#### Client certificate

You can create client certificate. To verify client by server.
(look at commented client certificate section in Apache2 site 
configuration file above).

```
    cd /opt/cfssl
    cfssl gencert \
              -ca=root/ca.pem \
              -ca-key=root/ca-key.pem \
              -config=root/ca-config.json \
              -profile=client 
              client.json \
    | cfssljson -bare client
```

But this is reasonable only if exist robust automatic way to distribute and renew 
client certificates on user workstations or on mobile devices. To generate it for 5 
or 10 years live time does not has a point. Because of very high chance that attacker can 
steal this certificate during so long period of time.


#### Certificate file types

Files:

- ca.pem  - certificate

  to inspect

```
  openssl req -text -noout -verify -in ca.csr
  openssl x509 -noout -in ca.pem -dates
  cfssl-certinfo -cert ca.pem
```

- ca-key.pem - private certificate key. 

**note: must be holded in secure and never leave node where it was generated**

- ca.csr  - certificate request

- <ca_unit>.crt  - certificate bundle to import on clients


### 3. Known backgrounds and issues

not found yet


### 4. Used documentation

https://blog.cloudflare.com/introducing-cfssl/

https://coreos.com/os/docs/latest/generate-self-signed-certificates.html

https://github.com/cloudflare/cfssl

https://github.com/cloudflare/certmgr
