# Init class
#
class drbd9 {


  $depencies = ['parted']
  $packages = ['drbd-utils']
  $services = ['drbd']

  # in openstack_controller.yaml there are list of node names without its IP addresses
  # Its IP addresses we are get from global/network.yaml by node names
  $network = lookup('network', Hash)

  $drbd = lookup('drbd9', Hash)
  $resources = $drbd['resources']


}

