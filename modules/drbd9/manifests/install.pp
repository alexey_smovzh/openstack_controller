# Install DRBD 9
# Creates decladed resources and brings it up
#
class drbd9::install inherits drbd9 {

  # check if it not already defined and install depencies
  $drbd9::depencies.each|$dep| {
    if !defined(Package[$dep]) {
      package { $dep:
        ensure => installed,
      }
    }
  }

  # install packages
  package { $drbd9::packages:
    ensure  => installed,
    require => Package[$drbd9::depencies]
  }

  # run service
  service { $drbd9::services:
    ensure  => running,
    enable  => true,
    require => Package[$drbd9::packages]
  }

  $drbd9::resources.each |$resource, $value| {

    $protocol = $value['protocol']
    $disk     = $value['disk']
    $device   = $value['device']
    $mount    = $value['mount']
    $initial  = $value['initial']
    $nodes    = $value['nodes']


    # Volume config
    file { "/etc/drbd.d/${resource}.res":
      ensure  => present,
      content => template('drbd9/resource.erb'),
      require => Package[$drbd9::packages]
    }

    # Create partition
    exec { "partition_${resource}":
      command => "/sbin/parted --script ${disk} \
                                        mklabel gpt \
                                        mkpart primary ext4 1MiB 100%",
      unless  => "/sbin/parted --script ${disk} print",
      require => File["/etc/drbd.d/${resource}.res"],
    }

    # Create device
    exec { "create_${resource}":
      command => "/usr/sbin/drbdadm create-md ${resource} --force",
      unless  => "/usr/sbin/drbdadm status ${resource}",
      require => Exec["partition_${resource}"],
    }

    # UP device
    exec { "up_${resource}":
      command => "/usr/sbin/drbdadm up ${resource}",
      require => Exec["create_${resource}"],
      unless  => "/usr/sbin/drbdadm status ${resource}",
    }

    # Point drdb which node is primary to begin syncronization
    if $drbd9::hostname == $drbd9::initial {
      # Secondary server can be not ready now
      # force this node to be 'primary' server
      # so we no need to wait when secondary node coming up.
      # otherwise next commands fails with errors
      exec { "force_${resource}":
        command => "/usr/sbin/drbdadm primary ${resource} --force",
        require => Exec["up_${resource}"],
        unless  => "/usr/sbin/drbdadm status ${resource} | /bin/grep UpToDate",
        notify  => Exec["delay_${resource}"]
      }

      # delay for 60 seconds
      # wait until drbd brings resource up
      exec { "delay_${resource}":
        command     => '/bin/sleep 60',
        refreshonly => true,
        notify      => Exec["mkfs_${resource}"]
      }

      # block device name are equal to resource name
      exec { "mkfs_${resource}":
        command     => "/sbin/mkfs.ext4 -F ${device}",
        require     => Exec["force_${resource}"],
        refreshonly => true,
        unless      => "/sbin/parted --script ${device} print | /usr/bin/grep ext4"
      }
    }
  }

}
