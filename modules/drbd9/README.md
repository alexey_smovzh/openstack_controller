### 0. Description
This module install DRBD 9


### 1. Tested environments
This module developed and tested on Debian 10 Buster


### 2. Usage
Describe 'drbd9' resources in proper Hiera yaml file:

```
  # Shared storage        
  drbd9:        
    resources:
      # resource name
      kvm_volume:
        # drbd replication protocol
        # for all available options look at 'Replication modes' 
        # chapter in official documentation
        protocol: C
        # physical disk for drbd replication
        disk: /dev/vdb
        # drbd device name
        device: /dev/drbd0
        # hostname of node to promote drbd device to primary state on
        initial: controller0
        # list of drbd cluster nodes hostnames
        nodes:
          - controller0
          - controller1
```

In 'drbd9' resource description there are no parameters to set node IP addresses.
This module rely on getting node IP addresses from another Hiera resource by node names.
In this setup it get its from 'global/network.yaml' 

```
  network:
    controller0:
      ip_forwarding: true
      ip_v6: false

      interfaces:
        br-ex:
          members: 
            - ens3  
          ip: 10.64.30.200/24                   
          gateway: 10.64.30.1
          dns: 8.8.8.8       
          mtu: 1500                   
          comment: OpenStack controller server
```

Install 

```
  include drbd9::install
```


### 3. Known backgrounds and issues
Not found yet.


### 4. Used documentation

DRBD documentation: https://docs.linbit.com/docs/users-guide-9.0/

Debian manual: https://wiki.debian.org/DrBd

Troubleshooting: http://avid.force.com/pkb/articles/en_US/Compatibility/Troubleshooting-DRBD-on-MediaCentral
