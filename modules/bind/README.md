### 0. Description
This module install Bind 9 DNS Server.


### 1. Tested environments
This module developed and tested on Debian 10.


### 2. Usage
Define configuration parameters in hiera yaml

```
    dns:
    name: dns-server
    address: 10.64.30.220
    network: 10.64.0.0/16
    zone: west
    reverse: 64.10 

    # ddns is optional parameter 
    # if you don't use it omit its definition
    ddns:
        # key generation
        # dnssec-keygen -a HMAC-MD5 -b 128 -r /dev/urandom -n USER DDNS_UPDATE
        # cat Kdhcp_updater.*.private
        secret: 4n27/6dy/t0zArxkArnv3g==

```

include into manifest

```
    include bind::install
```


### 3. Known backgrounds and issues
none


### 4. Used documentation

https://www.digitalocean.com/community/tutorials/how-to-configure-bind-as-a-private-network-dns-server-on-debian-9

ddns: https://wiki.debian.org/DDNS

https://bind9.readthedocs.io/en/v9_16_4/security.html

