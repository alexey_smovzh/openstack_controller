# Init class
#
class bind {

    $packages = ['bind9']
    $services = ['bind9']

    $puppet = lookup('puppet', Hash)

    $dns = lookup('dns', Hash)
    $ddns = $dns['ddns']
    $server = $dns['name']
    $address = $dns['address']
    $network = $dns['network']
    $zone = $dns['zone']
    $reverse = $dns['reverse']

}
