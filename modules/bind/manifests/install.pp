# Install Bind9
# Install Bind server, creates zone files from module templates
# ensures that service is enabled and running
#
class bind::install inherits bind {


  # install 
  package { $bind::packages:
    ensure => installed
  }

  # configure bind startup parameters
  file { '/etc/default/bind9':
    ensure  => present,
    content => template('bind/default.erb'),
    notify  => Service[$bind::services],
    require => Package[$bind::packages]
  }

  # zone configuration file
  # /etc/bind/named.conf.options
  # /etc/bind/named.conf.local
  # /var/lib/bind/db.west
  # /var/lib/bind/db.10.64  
  file { '/etc/bind/named.conf.options':
    ensure  => present,
    owner   => 'bind',
    group   => 'bind',
    content => template('bind/named.conf.options.erb'),
    notify  => Service[$bind::services],
    require => Package[$bind::packages]
  }

  file { '/etc/bind/named.conf.local':
    ensure  => present,
    owner   => 'bind',
    group   => 'bind',
    content => template('bind/named.conf.local.erb'),
    notify  => Service[$bind::services],
    require => Package[$bind::packages]
  }

  file { "/var/lib/bind/db.${bind::zone}":
    ensure  => present,
    owner   => 'bind',
    group   => 'bind',
    content => template('bind/db.zone.erb'),
    notify  => Service[$bind::services],
    require => Package[$bind::packages]
  }

  file { "/var/lib/bind/db.${bind::reverse}":
    ensure  => present,
    owner   => 'bind',
    group   => 'bind',
    content => template('bind/db.reverse.erb'),
    notify  => Service[$bind::services],
    require => Package[$bind::packages]
  }

  # write ddns key
  if $bind::ddns {
    file { '/etc/bind/ddns.key':
      ensure  => present,
      owner   => 'bind',
      group   => 'bind',
      mode    => '0640',
      content => template('bind/ddns.key.erb'),
      notify  => Service[$bind::services],
      require => Package[$bind::packages]
    }
  }

  # ensure service is running and enabled
  service { $bind::services:
    ensure  => running,
    enable  => true,
    require => Package[$bind::packages]
  }

}
