# Manage administrators group membership
# 
# @admins   - hash map of administrator accounts
# @group    - group administrators add to
# @depends  - requirements to run this function
#
define administrators::utils::group (

    Hash $admins,
    String $group,
    Any $depends

  ){

  $admins.each|$admin, $value| {

    exec {"add_administrator_to_group_${group}":
      command => "/usr/sbin/usermod -a -G ${group} ${admin}",
      unless  => "/usr/bin/groups ${admin} | /usr/bin/grep ${group}",
      require => $depends
    }

  }

}
