# Manage Linux user accounts
# 
# @admins   - hash map of administrator accounts
# @depends  - requirements to run this function
#
define administrators::utils::passwd (

    Hash $admins,
    Optional[Any] $depends = undef

  ){


  # if admin enabled add it to system
  # otherwise remove
  $admins.each|$admin, $value| {

    if $value['enabled'] == true {
      $state = 'present'
    } else {
      $state = 'absent'
    }

    # create user account
    user { $admin:
      ensure         => $state,
      groups         => 'sudo',
      comment        => $value['fullname'],
      home           => "/home/${admin}",
      shell          => '/usr/bin/bash',
      managehome     => true,
      purge_ssh_keys => true,
      password       => $value['password_crypted'],
    }

    # if ssh key provided
    if $value['ssh_key'] {

      ssh_authorized_key { $value['email']:
        ensure  => present,
        user    => $admin,
        type    => 'ssh-rsa',
        key     => $value['ssh_key'],
        require => User[$admin]
      }

    }

  }

}
