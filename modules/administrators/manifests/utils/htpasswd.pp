# Manage Apache htpasswd file 
# 
# @admins   - hash map of administrator accounts
# @path     - full path to .htpasswd file
# @depends  - requirements to run this function
#
define administrators::utils::htpasswd (

    Hash $admins,
    String $path,
    Optional[Any] $depends = undef

  ){


  # ensure .htpasswd file exist
  file { $path:
    ensure  => present,
    require => $depends
  }


  $admins.each|$admin, $value| {

    # if enabled ensure administrator exist
    if $value['enabled'] == true {

      exec {"add_administrator_${admin}":
        command => "/usr/bin/htpasswd -Bb ${path} ${admin} ${value['password']}",
        unless  => "/usr/bin/grep ${admin} ${path}",
        require => File[$path]
      }

      exec {"sync_administrator_password_${admin}":
        command => "/usr/bin/htpasswd -Bb ${path} ${admin} ${value['password']}",
        unless  => "/usr/bin/htpasswd -v -Bb ${path} ${admin} ${value['password']}",
        require => Exec["add_administrator_${admin}"]
      }

    # delete administrator
    } else {

      exec {"delete_administrator_${admin}":
        command => "/usr/bin/htpasswd -D ${path} ${admin}",
        onlyif  => "/usr/bin/grep ${admin} ${path}",
        require => File[$path]
      }

    }

  }

}
