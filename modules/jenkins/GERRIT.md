https://gerrit-review.googlesource.com/Documentation/intro-gerrit-walkthrough.html



ssh jenkins.west
sudo su - jenkins
mkdir .ssh
ssh-keygen -b 4096 -t rsa -m PEM -f .ssh/id_rsa -q -N ""

cat .ssh/id_rsa.pub

# add user to gerrit
ssh -i .ssh/id_rsa -p 29418 alex@gerrit.west gerrit create-account --group "'Non-Interactive Users'" --full-name Jenkins --email jenkins@jenkins --ssh-key "'<public ssh key>'" jenkins

# copy commit-msg hook to automaticaly add Change-Id to commit
scp -i .ssh/id_rsa -p -P 29418 alex@gerrit.west:hooks/commit-msg jenkins/.git/hooks/
chmod u+x jenkins/.git/hooks/commit-msg