
- In Jenkins create ssh user credentials
  (Manage Jenkins -> Manage Credentials -> Add Credentials (Username with password))
- Create new pipeline
  (New Item -> Pipeline)
- In pipeline section paste this example script

```
    node {
        stage('Build') {
            withCredentials([usernamePassword(credentialsId: 'openstack-creds', passwordVariable: 'password', usernameVariable: 'username')]) {
                sh "sshpass -p $password ssh -o 'StrictHostKeyChecking no' $username@10.64.30.100"
            }
        }
    }
```

- Save, run and check pipeline execution result

