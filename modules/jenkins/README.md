### 0. Description
This module install Jenkins CI/CD automation server.


### 1. Tested environments
This module developed and tested on Debian 10 Buster.


### 2. Usage
Install

```
  include jenkins::install
```

- After installation 'unlock' Jenkins and install desired plugins as described 
in this instructions https://www.jenkins.io/doc/book/installing/#unlocking-jenkins

- Create first administator account

- Configure Jenkins to use system PAM authorization
```
Manage Jenkins -> Configure Global Security -> Unix user/group database
```

- Log in with system cerdentials and delete administrator account from step 2


Integration examples:

- [Gogs](GOGS.md)
- [Gerrit](GERRIT.md)
- [ssh](SSH.md)


### 3. Known backgrounds and issues
none


### 4. Used documentation
https://www.jenkins.io/doc/book/installing/


