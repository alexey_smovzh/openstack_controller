
- Ensure that Jenkins Gogs plugin are installed
- Create Jenkins user in Gogs
- Create test repository in Gogs
- In repository create Jenkinsfile with Jenkins pipeline for this repository

```
    node {
        stage('Build') {
            echo 'Jenkins git test job successfull'
        }
    }
```

- In Jenkins create Gogs Jenkins user credentials 
  (Manage Jenkins -> Manage Credentials -> Add Credentials (Username with password)
- In Jenkins create new Pipeline with script source from SCM 
  (provide repository URL and credentials)
- In Gogs create webhook to trigger Jenkins pipeline on commit 
  (repository -> setting -> webhooks -> add new webhook)
  Where 'Payload URL' are http://<jenkins_ip>:<jenkins_port>/gogs-webhook/?job=<jenkins_job_name> 
  (for example: http://10.64.40.113:8080/gogs-webhook/?job=git_test_job)
- Make some commit to repository and check if gogs trigger pipeline in Jenkins


