# Init Class
#
class jenkins {


  $key = 'https://pkg.jenkins.io/debian-stable/jenkins.io.key'
  $repository = 'deb https://pkg.jenkins.io/debian-stable binary/'

  $depencies = ['git', 'openjdk-11-jre', 'gnupg', 'sshpass']
  $packages = ['jenkins']
  $services = ['jenkins']


}
