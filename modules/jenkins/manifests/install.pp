# Install jenkins
# Install packages required by Jenkins. Add Jenkins repository.
# Add Jenkins user to shadow group to allow PAM authentification.
# Ensures Jenkins service are enabled and running
#
class jenkins::install inherits jenkins {


  # check if it not already defined and install depencies
  $jenkins::depencies.each|$dep| {
    if !defined(Package[$dep]) {
      package { $dep:
        ensure => installed,
      }
    }
  }

  # create repository
  apt::utils::apt_repository { 'jenkins':
    key        => $jenkins::key,
    file       => '/etc/apt/sources.list.d/jenkins.list',
    repository => $jenkins::repository
  }


  # install 
  package { $jenkins::packages:
    ensure  => installed,
    require => [  Package[$jenkins::depencies],
                  Apt::Utils::Apt_repository['jenkins'] ]
  }

  # add user jenkins to group shadow for PAM jenkins user authorization
  exec { 'add_to_shadow_group':
    command => '/usr/sbin/usermod -a -G shadow jenkins',
    unless  => '/usr/bin/groups jenkins | /usr/bin/grep shadow',
    require => Package[$jenkins::packages]
  }

  # ensure service is running and enabled
  service { $jenkins::services:
    ensure  => running,
    enable  => true,
    require => Exec['add_to_shadow_group']
  }

}
