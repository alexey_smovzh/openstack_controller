# create APT repository by downloading and installing deb package
# @deb      - Link to download Deb repository package
# @package  - Name of the package file
# @fullname - Full name of package
#
define apt::utils::apt_repository_deb (String $deb, String $package, String $fullname) {

  # download repo deb package
  exec { "${title}_download_deb":
    command => "/usr/bin/wget ${deb}",
    cwd     => '/tmp',
    creates => "/tmp/${package}",
    # Check if repository deb package already installed and don't run exec tasks if it true
    unless  => "/usr/bin/apt-cache show ${fullname}",
  }

  package { $package:
    ensure   => installed,
    provider => 'dpkg',
    source   => "/tmp/${package}",
    require  => Exec["${title}_download_deb"],
  }

  exec { "${title}_apt_update":
    command => '/usr/bin/apt update',
    require => Package[$package],
  }
}
