# Update sources
define apt::utils::apt_update {

  exec { "${title}_update":
    command => '/usr/bin/apt update',
  }
}
