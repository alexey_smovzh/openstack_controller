# create APT repository by creting repo file manually
#
# @file - repository file in /etc/apt/sources.list.d/
# @repository - content of repository file
# @key - Key ID or http:// link to key
#
define apt::utils::apt_repository(String $file, String $repository, $key = undef) {

  # Test key value
  case $key {
    # http link
    /^http+/: {
      $command = "/usr/bin/wget -qO - ${key} | apt-key add -"
    }
    # key ID
    /^[A-Z0-9]+/: {
      $command = "/usr/bin/apt-key adv --keyserver keyserver.ubuntu.com --recv-keys ${key}"
    }
    # default empty
    default: {}
  }

  # Check if key passed
  if $command != undef {
    exec { "${title}_key_install":
      command => $command,
      cwd     => '/tmp',
      # Check if repository file already exists and don't run exec tasks if it true
      unless  => "/usr/bin/test -f ${file}",
    }
  }

  # create repository
  # echo "$repository" | sudo tee -a $file
  file { $file:
    ensure  => present,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    replace => 'false',
    content => $repository,
    notify  => Exec["${title}_apt_update"],
  }

  exec { "${title}_apt_update":
    command     => '/usr/bin/apt update',
    require     => File[$file],
    refreshonly => true,
  }
}
