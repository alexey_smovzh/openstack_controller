# Init class
#
class consul {


  $depencies = ['wget', 'unzip', 'curl']
  $services = ['consul']

  $user = 'consul'
  $group = 'consul'

  $consul = lookup('consul', Hash)
  $datacenter = $consul['datacenter']
  $home = $consul['home']
  $link = $consul['link']
  $key = $consul['key']
  $nodes = $consul['nodes']


}
