# Install Consul service
# Creates 'consul' user and group. Downloads consul binary
# from provided in Hiera configuration link. Creates configuration
# file from template. Creates service file and ensures that service
# are enabled and runned
#
class consul::install inherits consul {


  # check if it not already defined and install depencies
  $consul::depencies.each|$dep| {
    if !defined(Package[$dep]) {
      package { $dep:
        ensure => installed,
      }
    }
  }

  # create vault user and group
  group { $consul::group:
    ensure => present,
    system => true
  }

  user { $consul::user:
    ensure     => 'present',
    home       => $consul::home,
    shell      => '/bin/false',
    groups     => $consul::group,
    comment    => 'Hashicorp Consul',
    password   => '*',
    managehome => true,
    require    => Group[$consul::group]
  }

  # extract archive name from download link
  $archive = regsubst($consul::link, '^(.*[\\\/])', '')

  # install consul 
  exec { 'install_consul':
    command  => "/usr/bin/wget -q ${consul::link} \
              && /usr/bin/unzip -j ${archive}",
    provider => 'shell',
    cwd      => $consul::home,
    user     => $consul::user,
    group    => $consul::group,
    creates  => "${consul::home}/consul",
    require  => [ Package[$consul::depencies],
                  User[$consul::user] ]
  }

  # add consul folder to PATH
  file { '/etc/profile.d/consul.sh':
    ensure  => present,
    owner   => 'root',
    group   => 'root',
    content => "export PATH=\"\${PATH}:${consul::home}\"",
    require => Exec['install_consul']
  }

  # create consul config folder
  file { "${consul::home}/conf.d":
    ensure  => 'directory',
    owner   => $consul::user,
    group   => $consul::group,
    recurse => true,
    require => User[$consul::user]
  }

  # create consul config
  file { "${consul::home}/conf.d/consul.hcl":
    ensure  => present,
    content => template('consul/consul.hcl.erb'),
    owner   => $consul::user,
    group   => $consul::group,
    notify  => Service[$consul::services],
    require => [  File["${consul::home}/conf.d"],
                  Exec['install_consul']  ]
  }

  # create service file
  file { '/lib/systemd/system/consul.service':
    ensure  => present,
    content => template('consul/consul.service.erb'),
    notify  => Service[$consul::services],
    require => Exec['install_consul']
  }

  # ensure service is running and enabled
  service { $consul::services:
    ensure  => running,
    enable  => true,
    require => [  File["${consul::home}/conf.d/consul.hcl"],
                  File['/lib/systemd/system/consul.service']  ]
  }

}
