### 0. Description
This module install HashiCorp Consul distributed 
key-value storage


### 1. Tested environments
This module developed and tested on Debian 10 Buster


### 2. Usage
Describe cluster configuration in Hiera yaml file:

```
  consul:
    # consul cluster name
    datacenter: 'controller'
    # consul user home directory and directory 
    # for consul binaries and configuration files
    home: '/opt/consul'
    # URL for latest stable consul binaries download
    link: 'https://releases.hashicorp.com/consul/1.8.5/consul_1.8.5_linux_amd64.zip'
    # key to secure message transfer between cluster members
    # $ consul keygen
    key: 'Ok02dpEgd+VlxywNBAKUBaVm0xxgIbpP96cGMmjy4ro='
    # cluster members hostnames
    nodes:
      - controller0
      - controller1
      - monitoring0
```

*note: for proper functioning this module required preconfigured*
*cluster members IP address by hostname resolution*


Install
```
  include consul::install
```


Consul web ui: https://<any_member_ip>:8500


### 3. Known backgrounds and issues
not found


### 4. Used documentation

https://learn.hashicorp.com/tutorials/consul/deployment-guide
