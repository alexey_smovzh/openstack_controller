# Init Class
#
class vault {

  $depencies = ['wget', 'unzip']
  $services  = ['vault']

  $user  = 'vault'
  $group = $user

  $vault = lookup('vault', Hash)
  $home  = $vault['home']
  $link  = $vault['link']

}
