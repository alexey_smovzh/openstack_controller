# Install HashiCorp Vault
# Install required packages for Vault. Create Vault user, group
# and create home and storage folders. Download and uppack
# Vault binary. Create configuration file from template. Create
# systemd service file and ensure service are enabled and running.
# 
class vault::install inherits vault {


  # check if it not already defined and install depencies
  $vault::depencies.each|$dep| {
    if !defined(Package[$dep]) {
      package { $dep:
        ensure => installed,
      }
    }
  }

  # create vault user and group
  group { $vault::group:
    ensure => present,
    system => true
  }

  user { $vault::user:
    ensure     => 'present',
    home       => $vault::home,
    shell      => '/bin/false',
    groups     => $vault::group,
    comment    => 'Hashicorp Vault',
    password   => '*',
    managehome => true,
    require    => Group[$vault::group]
  }

  # create vault store folder
  file { '/opt/vault/data':
    ensure  => 'directory',
    owner   => $vault::user,
    group   => $vault::group,
    require => User[$vault::user]
  }

  # extract archive name from download link
  $archive = regsubst($vault::link, '^(.*[\\\/])', '')

  # install vault 
  exec { 'install_vault':
    command  => "/usr/bin/wget ${vault::link} \
              && /usr/bin/unzip -j ${archive}",
    provider => 'shell',
    cwd      => $vault::home,
    user     => $vault::user,
    group    => $vault::group,
    creates  => "${vault::home}/vault",
    require  => [ Package[$vault::depencies],
                  User[$vault::user]  ]
  }

  # add vault folder to PATH
  file { '/etc/profile.d/vault.sh':
    ensure  => present,
    owner   => 'root',
    group   => 'root',
    content => "export PATH=\"\${PATH}:${vault::home}\"",
    require => Package[$vault::depencies]
  }

  # create vault config
  file { "${vault::home}/vault.hcl":
    ensure  => present,
    content => template('vault/vault.hcl.erb'),
    owner   => $vault::user,
    group   => $vault::group,
    notify  => Service[$vault::services],
    require => Exec['install_vault']
  }

  # create service file
  file { '/lib/systemd/system/vault.service':
    ensure  => present,
    content => template('vault/vault.service.erb'),
    notify  => Service[$vault::services],
    require => Exec['install_vault']
  }

  # ensure service is running and enabled
  service { $vault::services:
    ensure  => running,
    enable  => true,
    require => [  File["${vault::home}/vault.hcl"],
                  File['/lib/systemd/system/vault.service'] ]
  }

}
