### 0. Description
Install Hashicorp Vault


### 1. Tested environments
Debian 10


### 2. Usage
Provide vault home and vault download link in Hiera

```
    vault:
        home: '/opt/vault'
        link: 'https://releases.hashicorp.com/vault/1.5.4/vault_1.5.4_linux_amd64.zip'

```

install

```
    include vault::install
```

configure via UI http://vault:8200/ui
or from command line

```
    # use http until https not configured
    export VAULT_ADDR='http://127.0.0.1:8200'

    # init vault
    # keys.txt will contain keys to unseal vault
    vault operator init > keys.txt

    # unseal vault after initialization
    vault operator unseal

    # login as root
    vault login <root token from keys.txt>

    # enable user/password auth
    vault auth enable userpass

    # add user
    vault write auth/userpass/users/alex password=alex policies=admins

    # enable SSH certificate auth
    vault secrets enable -path=ssh-cert -description='CA authority for ssh certificate based access' ssh 
    vault write ssh-cert/config/ca generate_signing_key=true

    # create Vault role 
    vault write ssh-cert/roles/ssh-role -<<"EOH"
    {
    "allow_user_certificates": true,
    "allowed_users": "*",
    "allowed_extensions": "permit-pty,permit-port-forwarding,permit-user-rc",
    "default_extensions": [
        {
        "permit-pty": "",
        "permit-user-rc": ""
        }
    ],
    "key_type": "ca",
    "default_user": "debian",
    "ttl": "30m0s"
    }
    EOH

    # download public Vault certificate on managed systems
    curl -o /etc/ssh/trusted-user-ca-keys.pem http://vault:8200/v1/ssh-cert/public_key

    # update managed system SSH server config to auth by Vault certificate
    sudo vi /etc/ssh/sshd_config

        TrustedUserCAKeys /etc/ssh/trusted-user-ca-keys.pem

    # on administrator workstation generate certificate
    ssh-keygen -t rsa -C "user@example.com"

    # sign it via Vault UI and save signed public certificate under user .ssh/ folder
# todo: path in UI

    # login to managed system
    ssh -i ~/.ssh/signed-cert.pub -i ~/.ssh/id_rsa <username>@<managed_system>

```


### 3. Known backgrounds and issues
not found yet


### 4. Used documentation

https://www.vaultproject.io/docs/secrets/ssh/signed-ssh-certificates

https://learn.hashicorp.com/tutorials/vault/pki-engine

https://www.ibm.com/support/knowledgecenter/SSHKN6/cert-manager/3.x.x/cert_vault.html

https://medium.com/hashicorp-engineering/pki-as-a-service-with-hashicorp-vault-a8d075ece9a
