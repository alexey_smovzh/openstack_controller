### 0. Description
This module install Gitea - lightweight code hosting solution.

Gitea developers does not provide gitea binary with 
PAM authentification support. But in this module exactly 
this authentification method are used. So we need to 
build Gitea from source with PAM auth enabled. 
Build instructions are in 'Usage' section.


### 1. Tested environments
This module developed and tested on Debian 10 Buster.


### 2. Usage
Build from source

```
    # install make, gcc, libc-dev etc.
    sudo apt install git build-essential libpam0g-dev -y

    # install Go
    wget https://golang.org/dl/go1.15.5.linux-amd64.tar.gz
    sudo tar -C /opt/ -xzf go1.15.5.linux-amd64.tar.gz 
    export PATH=$PATH:/opt/go/bin
    go version

    # install Node.js with npm
    wget https://nodejs.org/dist/v14.15.1/node-v14.15.1-linux-x64.tar.xz
    sudo tar -C /opt/ -xf ./node-v14.15.1-linux-x64.tar.xz
    export PATH=$PATH:/opt/node-v14.15.1-linux-x64/bin
    node -v

    # Build Gitea
    git clone https://github.com/go-gitea/gitea -b release/v1.12 --depth 1
    cd gitea/
    TAGS="bindata pam" make build

    # copy gitea binary to Puppet module
    scp <user>@<build-host>:/<gitea-binary> openstack.controller/modules/gitea/files
```


Provide Gitea  parameters in Hiera yaml file

```
    gitea:
        # Gitea binary file name
        # For all possible variants look at 'gitea/files' folder
        version: 'gitea_v1.13'
        home: '/opt/gitea'
        # port to run http UI interface
        # default is 3000
        port: 80
        # database connection information
        dbhost: 127.0.0.1
        dbuser: *user
        dbpass: *pass
        # postinstall configuration tasks
        postinstall:
            # add users from 'global/administrators.yaml'
            users: true
```

Then install.
This module require pre-installed MariaDB or MySQL database server.
To ensure the database are operational before Gitea installation
it usefull to use Puppet anchoring.

```
    # need to be sure mariadb are installed before gitea
    include mariadb::install
    include gitea::install

    Class['mariadb::install'] ->
    Class['gitea::install']
```

#### Postinstall configuration

- In user settings import user ssh key

```
    Settings -> SSH / GPG keys -> Add key
```

- create and initialize repository with default README.md

- add branch protection to disable direct push to master
configure reviewers, min number of reviewer for pull request and other settings

```
<repository> -> settings -> Branches
```

- On administrator worstation clone repository
    
```    
    git clone ssh://git@gitea.west:2222/alex/production.git
```

- create new branch for new feature

```
    git branch feature_23
    git checkout feature_23
    <make some changes>
    git add <new_files>
    git commit -m 'add new feature number 23'
    git push -u origin feature_23
```

- create pull request, add reviewers, review code, merge to master


How to configure integration with Drone CI service read [GITEA.md](../drone/GITEA.md)


### 3. Known backgrounds and issues
not found yet


### 4. Used documentation
Gitea documentation: https://docs.gitea.io/en-us/
