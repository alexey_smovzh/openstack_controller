# Manage Gitea user account
# 
# @admins   - hash map of administrator accounts
# @home     - full path to gitea home folder
# @version  - gitea executable name with version code
# @dbuser   - giteadb user name
# @dbpass   - password for giteadb user
# @depends  - requirements to run this function
#
define gitea::utils::account (

    Hash $admins,
    String $home,
    String $version,
    String $dbuser,
    String $dbpass,
    Optional[Any] $depends = undef

  ){

  # name of PAM auth method
  # it are used in several places so we want to be sure 
  # it has the same value in all of it
  $pam_name = 'System Auth'

  # we are create all users without dependency on its status (enabled/disabled)
  # since we are use PAM auth, if user will be disabled, it will be disabled on 
  # system level and can't login to Gitea
  # In other hand, if user has some repositories already, we can't delete it 
  # from Gitea in any case
  # So strategy to create all users despite its states are conform all this circumstances
  #
  $admins.each |$admin, $value| {

    # All two commands writes its data to mariadb database
    # some times can occur deadlock situation because they do it very fast
    # to avoid manifest fails with error, there are configured retries
    # this little hack avoid problem with deadlocks very well

    # create user
    exec { "greate_gitea_user_${admin}":
      command     => "${home}/${version} admin create-user \
                                               --username ${admin} \
                                               --password ${value['password']} \
                                               --email ${value['email']} \
                                               --admin \
                                               -c ${home}/app.ini",
      environment => ["HOME=${home}"],
      provider    => 'shell',
      tries       => 3,
      try_sleep   => 2,
      unless      => "/usr/bin/mariadb -u ${dbuser} -p${dbpass} giteadb -e 'select name from user;' \
                    | /usr/bin/grep ${admin}",
      require     => $depends
    }

    # update user login source
    exec { "configure_login_source_for_${admin}":
      command   => "/usr/bin/mariadb -u ${dbuser} -p${dbpass} giteadb \
                                   -e \"update user \
                                        set login_source = (select id from login_source where name = '${pam_name}'), \
                                        login_name = '${admin}', \
                                        must_change_password = '0', \
                                        login_type = '4' \
                                        where name = '${admin}';\"",
      provider  => 'shell',
      tries     => 3,
      try_sleep => 2,
      unless    => "/usr/bin/mariadb -u ${dbuser} -p${dbpass} giteadb \
                                     -e \"select name from user \
                                          where login_source = (select id from login_source where name = '${pam_name}');\" \
                  | /usr/bin/grep ${admin}",
      require   => Exec["greate_gitea_user_${admin}"]
    }

  }

}

