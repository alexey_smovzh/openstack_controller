# Postinstall task configured in Hiera
#
class gitea::postinstall inherits gitea {


  # manage users accounts
  if $gitea::postinstall['users'] == true {

    gitea::utils::account { 'manage_gitea_user_accounts':
      admins  => lookup('administrators', Hash),
      home    => $gitea::home,
      version => $gitea::version,
      dbuser  => $gitea::dbuser,
      dbpass  => $gitea::dbpass
    }

  }

}
