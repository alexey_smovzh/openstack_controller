# Install Gitea
# Install packages reqired by Gitea. Create system Gitea user and group.
# Add Gitea user to system group 'shadow' to enable PAM authentification.
# Create home folder and folder hierarchy inside it. Copy Gitea binary
# from module files folder. Upload database schema to SQL server. 
# Create systemd service file and ensure service are enabled and running.
# Perfom postistall tasks if it enabled.
#
class gitea::install inherits gitea {


  # check if it not already defined and install depencies
  $gitea::depencies.each|$dep| {
    if !defined(Package[$dep]) {
      package { $dep:
        ensure => installed,
      }
    }
  }

  # create gitea user and group
  group { $gitea::group:
    ensure => present,
    system => true
  }

  user { $gitea::user:
    ensure     => 'present',
    home       => $gitea::home,
    shell      => '/bin/false',
    groups     => $gitea::group,
    comment    => 'Git Version Control',
    password   => '*',
    managehome => true,
    require    => Group[$gitea::group]
  }

  # add Git user to group shadow for PAM authorization
  exec { 'add_to_shadow_group':
    command => "/usr/sbin/usermod -a -G shadow ${gitea::user}",
    unless  => "/usr/bin/groups ${gitea::user} | /usr/bin/grep shadow",
    require => User[$gitea::user]
  }

  # create folder for store gitea data
  file { [  "${gitea::home}/custom",
            "${gitea::home}/data",
            "${gitea::home}/data/lfs",
            "${gitea::home}/gitea-repositories",
            "${gitea::home}/log"  ]:
    ensure  => 'directory',
    owner   => $gitea::user,
    group   => $gitea::group,
    recurse => true,
    require => Exec['add_to_shadow_group']
  }

  # Copy binaries and database scheme
  file { 'copy_gitea_files':
    path    => $gitea::home,
    source  => 'puppet:///modules/gitea',
    owner   => $gitea::user,
    group   => $gitea::group,
    mode    => '0755',
    recurse => true,
    require => User[$gitea::user]
  }

  # add gitea folder to PATH
  file { '/etc/profile.d/gitea.sh':
    ensure  => present,
    owner   => 'root',
    group   => 'root',
    content => "export PATH=\"\${PATH}:${gitea::home}\"",
    require => File['copy_gitea_files']
  }

  # create configuration file
  file { "${gitea::home}/app.ini":
    ensure  => present,
    owner   => $gitea::user,
    group   => $gitea::group,
    content => template('gitea/app.ini.erb'),
    notify  => Service[$gitea::services],
    require => File['copy_gitea_files']
  }

  # create database
  exec { 'import_database_schema':
    command  => "/usr/bin/mariadb -u ${gitea::dbuser} -p${gitea::dbpass} giteadb < ${gitea::home}/gitea-db.sql",
    provider => 'shell',
    user     => 'root',
    group    => 'root',
    onlyif   => "/usr/bin/mariadb -u ${gitea::dbuser} -p${gitea::dbpass} giteadb -e 'check table notification;' | /usr/bin/grep Error",
    require  => File['copy_gitea_files']
  }

  # create service file
  file { '/lib/systemd/system/gitea.service':
    ensure  => present,
    content => template('gitea/service.erb'),
    notify  => Service[$gitea::services],
    require => File['copy_gitea_files']
  }

  # ensure service is running and enabled
  service { $gitea::services:
    ensure  => running,
    enable  => true,
    require => [  File['/lib/systemd/system/gitea.service'],
                  Exec['import_database_schema']  ]
  }


  # if enabled run postinstall configuration tasks
  if $gitea::postinstall {
    include gitea::postinstall
  }

}
