# Init Class
#
class gitea {

  # hiera values
  $gitea = lookup('gitea', Hash)
  $version = $gitea['version']
  $home = $gitea['home']
  $port = $gitea['port']
  $dbhost = $gitea['dbhost']
  $dbuser = $gitea['dbuser']
  $dbpass = $gitea['dbpass']
  # postinstall configuration tasks
  $postinstall = $gitea['postinstall']


  $depencies = ['git']
  $services = ['gitea']


  $user = 'git'
  $group = $user


}
