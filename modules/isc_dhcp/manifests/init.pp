# Init Class
#
class isc_dhcp {

    $packages = ['isc-dhcp-server']
    $services = ['isc-dhcp-server']

    $dns = lookup('dns', Hash)
    $dhcp = lookup('isc_dhcp', Hash)
    $interface = $dhcp['interface']
    $networks = $dhcp['networks']

}

