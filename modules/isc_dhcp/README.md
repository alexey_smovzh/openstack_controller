### 0. Description
This module install isc-dhcp-server.


### 1. Tested environments
This module developed and tested on Debian 10.


### 2. Usage
Define configuration parameters in hiera yaml

```
    # DHCP server
    isc_dhcp:
        # interface to listen on
        interface: *interface
        
        # optional parameter
        # if you don't need DDNS just skip it
        ddns: 
            # commands to generate key
            # $ dnssec-keygen -a HMAC-MD5 -b 128 -r /dev/urandom -n USER DDNS_UPDATE
            # $ cat Kdhcp_updater.*.private
            secret: mCzPGMAluZkO1LBIqEUQ4w==

        networks:
            # network name
            # last octet - network mask (i.e. 10.64.30.0/24)
            30-24:         
                subnet: 10.64.30.0
                netmask: 255.255.255.0
                gw: 10.64.30.1
                # pool range
                # first and last addresses
                start: 10.64.30.230
                end: 10.64.30.249
            
                # optional parameter    
                # assign ip address by hostname
                # if don't need this feature skip it
                static:
                    puppet-server:
                        address: 10.64.30.210
                    dns-server: 
                        address: 10.64.30.220  
```

include into manifest

```
    include isc_dhcp::install
```


### 3. Known backgrounds and issues
none


### 4. Used documentation

Ip address assignment by host name: https://community.infoblox.com/t5/DNS-DHCP-IPAM/Is-it-possibble-to-do-DHCP-Reservation-by-hostname-not-by-mac/td-p/11345

ddns: https://wiki.debian.org/DDNS